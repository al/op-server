package com.exception;

/**
 * @author yt
 * @createtime 2018-08-10 11:06:23
 * @description
 */
public class AppException extends RuntimeException {

    private String code;
    private String message;

    public AppException() {
    }

    public AppException(String message) {
        super(message);
        this.message = message;
    }

    public AppException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}