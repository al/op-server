package com.exception;

/**
 * @author yt
 * @createtime 2018-08-10 11:11:00
 * @description
 */
public enum ExceptionConst {

    EXCEPTION_CODE_0000("0000", "服务器错误！"),
    EXCEPTION_CODE_0001("0001", "人员登录名已经被注册！"),
    ;

    private String code;
    private String message;

    ExceptionConst(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
