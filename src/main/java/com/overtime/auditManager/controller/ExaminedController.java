package com.overtime.auditManager.controller;

import com.overtime.auditManager.bean.Examined;
import com.base.bean.RespBean;
import com.overtime.auditManager.service.ExaminedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zhong on 2018/8/13.
 * 审核人信息管理
 */
@RestController
@RequestMapping("/examined")
public class ExaminedController {
    @Autowired
    ExaminedService examinedService;
    //获取全部审核人信息
    @RequestMapping("/getExamined")
    public List examined(Integer start,Integer pageSize,@RequestParam("name") String name){

        return examinedService.getExamined(start,pageSize,name);
    }
    @RequestMapping("/getExaminedTotal")
    public List getEaminedTotal(@RequestParam("name")String name){
        List list = examinedService.getEaminedTotal(name);
        return examinedService.getEaminedTotal(name);
    }
    //修改审核人信息
    @RequestMapping("/editeExamined")
    public RespBean editeExamined(Examined examined){
        if(examinedService.editeExamined(examined)==1){
            return new RespBean("success","修改成功");
        }
        return new RespBean("error","修改失败");
    }
    //获取职务
    @RequestMapping("/getDuty")
    public List getDuty(){
        return examinedService.getDuty();
    }
    //通过审核人名称模糊查询审核人信息
//    @RequestMapping("/findByName")
//    public List findByName(String name){
//        return examinedService.findByName(name);
//    }

    @RequestMapping("/getUserByDepno")
    public List getUserByDepno(int depno){
        List list =examinedService.getUserByDepno(depno);
        return examinedService.getUserByDepno(depno);
    }

    @RequestMapping("/addPE")
    public RespBean addPE(Examined examined){
        if(examinedService.findByName(examined.getUser_id()).size()>0){
            return new RespBean("error","当前审核人已存在");
        }
        if(examinedService.addPE(examined)==1){
            return new RespBean("success","新增成功");
        }else{
            return new RespBean("error","新增失败");
        }
    }

    @RequestMapping("/deleteExamined")
    public RespBean deleteExamined(int audit_manager_id){
        if(examinedService.selectRelate(audit_manager_id).size()>0){
            return new RespBean("warning","当前审核人已存在审核序列,请先删除审核等级中对应审核人");
        }
        if(examinedService.deleteExamined(audit_manager_id)==1){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }
    @RequestMapping("/deletePEs")
    public RespBean deletePEs(String ids){
        String[] strings = ids.split(",");
        int sum = 0;
        for(String str:strings) {
            int id = Integer.valueOf(str);
            if(examinedService.selectRelate(id).size()>0){
                return new RespBean("warning","当前审核人已存在审核序列,请先删除审核等级中对应审核人");
            }
        }
        for(String str:strings){
            int id = Integer.valueOf(str);
            sum+=examinedService.deleteExamined(id);
        }
        if(sum==strings.length){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }

}
