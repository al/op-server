package com.overtime.auditManager.controller;

import com.base.bean.RespBean;
import com.overtime.auditManager.service.AuditLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/15.
 * 审核等级管理
 */
@RestController
@RequestMapping("/auditLevel")
public class AuditLevelController {
    @Autowired
    AuditLevelService auditLevelService;

    @RequestMapping("/addAL")
    public RespBean addAL(String level){
        if(auditLevelService.addAL(level)==1){
            return new RespBean("success","新增成功");
        }else{
            return new RespBean("error","新增失败");
        }

    }
    @RequestMapping("/getAL")
    public List<Map> getAL(){
        return auditLevelService.getAL();
    }

    @RequestMapping("/getExaminedByAL")
    public List<Map> getExaminedByAL(String audit_level_id){
        return auditLevelService.getExaminedByAL(audit_level_id);
    }

    @RequestMapping("/addAR")
    public RespBean addAR(String audit_level_id,String ar_order,String audit_manager_id){
        Map map = new HashMap();
        map.put("audit_level_id",audit_level_id);
        map.put("audit_manager_id",audit_manager_id);
        map.put("ar_order",ar_order);
        if(auditLevelService.getAR(map).size()>0){
            return new RespBean("error","已存在此记录");
        }
        if(auditLevelService.addAR(map)==1){
            return new RespBean("success","新增成功");
        }else{
            return new RespBean("error","新增失败");
        }
    }
    @RequestMapping("/getExamined")
    public List<Map> getExamined(){
        return auditLevelService.getExamined();
    }
    @RequestMapping("/deleteAL")
    public RespBean deleteAL(String audit_level_id){
        if(auditLevelService.deleteAL(audit_level_id)==1){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }
    @RequestMapping("/deleteAR")
    public RespBean deleteAR(String ar_id){
        if(auditLevelService.deleteAR(ar_id)==1){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }
}
