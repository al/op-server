package com.overtime.auditManager.mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/15.
 */
public interface AuditLevelMapper {
    int addAL(String level);
    List<Map> getAL();
    List<Map> getExaminedByAL(String audit_level_id);
    int addAR(Map map);
    List<Map> getExamined();
    int deleteAL(String audit_level_id);
    int deleteAR(String ar_id);
    int deleteARByAL(String audit_level_id);
    List<Map> getAR(Map map);
}
