package com.overtime.auditManager.mapper;

import com.overtime.auditManager.bean.Examined;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/13.
 */
public interface ExaminedMapper {
    List getExamined(Map map);
    int editeExamined(Examined examined);
    List getDuty();
    List findByName(String userid);
    List getUserByDepno(int depno);
    int addPE(Examined examined);
    int deleteExamined(int audit_manager_id);
    List selectRelate(int audit_manager_id);
    List getEaminedTotal(String name);
}
