package com.overtime.auditManager.service;

import com.overtime.auditManager.mapper.AuditLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/15.
 */
@Service
@Transactional
public class AuditLevelService {
    @Autowired
    AuditLevelMapper auditLevelMapper;

    public int addAL(String level){
        return auditLevelMapper.addAL(level);
    }

    public List<Map> getAL(){
        return auditLevelMapper.getAL();
    }

    public List<Map> getExaminedByAL(String map){
        return auditLevelMapper.getExaminedByAL(map);
    }

    public int addAR(Map map){
        return auditLevelMapper.addAR(map);
    }

    public List<Map> getExamined(){
        return auditLevelMapper.getExamined();
    }
    public List<Map> getAR(Map map){
        return auditLevelMapper.getAR(map);
    }
    public int deleteAL(String audit_level_id){
        auditLevelMapper.deleteARByAL(audit_level_id);
        return auditLevelMapper.deleteAL(audit_level_id);
    }
    public int deleteAR(String ar_id){
        return auditLevelMapper.deleteAR(ar_id);
    }
}
