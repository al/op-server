package com.overtime.auditManager.service;

import com.overtime.auditManager.bean.Examined;
import com.overtime.auditManager.mapper.ExaminedMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/13.
 */
@Service
@Transactional
public class ExaminedService {
    @Autowired
    ExaminedMapper examinedMapper;
    public List getExamined(Integer start,Integer pageSize,String name){
        Map map = new HashMap();
        map.put("start",start);
        map.put("pageSize",pageSize);
        map.put("name",name);
        return examinedMapper.getExamined(map);
    }
    public List getEaminedTotal(String name){
        return examinedMapper.getEaminedTotal(name);
    }
    public int editeExamined(Examined examined){
        return examinedMapper.editeExamined(examined);
    }

    public List getDuty(){
//        Map map = new HashMap();
//        Map map1 = new HashMap();
//        map.put("id","1");
//        map.put("name","管理员");
//        map1.put("id","2");
//        map1.put("name","超级管理员");
//        List list = new ArrayList();
//        list.add(map);
//        list.add(map1);
//        return list;
        return examinedMapper.getDuty();
    }
    public List findByName(String name){
        return examinedMapper.findByName(name);
    }

    public List getUserByDepno(int depno){
        return examinedMapper.getUserByDepno(depno);
    }

    public int addPE(Examined examined){
        int i=examinedMapper.addPE(examined);
        System.out.println(examined.getAudit_manager_id());
        return i;
    }
    public int deleteExamined(int audit_manager_id){
        return examinedMapper.deleteExamined(audit_manager_id);
    }

    public List selectRelate(int audit_manager_id){
        return examinedMapper.selectRelate(audit_manager_id);
    }
}
