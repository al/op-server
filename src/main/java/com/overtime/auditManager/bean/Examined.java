package com.overtime.auditManager.bean;

/**
 * Created by zhong on 2018/8/14.
 */
public class Examined {
    private int audit_manager_id;
    private String user_id;
    private String user_name;
    private String audit_manager_duty;

    public int getAudit_manager_id() {
        return audit_manager_id;
    }

    public void setAudit_manager_id(int audit_manager_id) {
        this.audit_manager_id = audit_manager_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAudit_manager_duty() {
        return audit_manager_duty;
    }

    public void setAudit_manager_duty(String audit_manager_duty) {
        this.audit_manager_duty = audit_manager_duty;
    }
}
