package com.overtime.account.mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/28.
 */
public interface AccountTableMapper {
    List getUser(Map depno);
    List getMoney(Map map);
    List getUserByExpenseId(int[] expenseIds);
    List getMoneyByExpenseId(Map map);
}
