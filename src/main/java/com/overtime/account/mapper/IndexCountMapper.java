package com.overtime.account.mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/11/19 0019.
 */
public interface IndexCountMapper {
    List<Map<String,Object>> expenseCount(Map<String, Object> map);

    List<Map<String,Object>> applicationCount(Map<String, Object> map);

    List<Map<String,Object>> projectCount(Map<String, Object> map);
}
