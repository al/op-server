package com.overtime.account.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/22.
 */
@Mapper
public interface AccountMoneyMapper {
    List<Map<String,String>> getTotalDataByYear(String time);
    List<Map<String,String>> getTotalDataByMonth(String time);
    List<Map<String,String>> getTotalData();
    List getTotalData1();
    List getTotalDataByYear1(String time);
    List getTotalDataByMonth1(String time);
}
