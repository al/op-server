package com.overtime.account.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: 徐双翎
 * Date: 2020/1/18
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 * Description:mapper接口，统计数据
 */
public interface AmountMapper {
    /**
     * 加班总天数、加班地点数、加班项目数、加班人员数查询
     *
     * @param year 通过年份查询
     * @param date 通过月份查询
     * @return 返回查询结果集，失败返回长度为0的list集合
     */
    List<Map<String, Object>> selectCountDay(@Param("year") String year, @Param("date") String date);

    /**
     * 工作日、节假日查询
     *
     * @param year 通过年份查询
     * @param date 通过月份查询
     * @return 返回查询结果集，失败返回长度为0的list集合
     */
    List<Map<String, Object>> selectWorkAndHoliday(@Param("year") String year, @Param("date") String date);

    /**
     * 人员统计
     *
     * @param year 通过年份查询
     * @param date 通过月份查询
     * @return 返回查询结果集，失败返回长度为0的list集合
     */
    List<Map<String, Object>> selectByPerson(@Param("year") String year, @Param("date") String date);

    /**
     * 报销统计
     *
     * @param year 通过年份查询
     * @param date 通过月份查询
     * @return 返回查询结果集，失败返回长度为0的list集合
     */
    List<Map<String, Object>> selectByPay(@Param("year") String year, @Param("date") String date);

    /**
     * 项目统计
     *
     * @param year 通过年份查询
     * @param date 通过月份查询
     * @return 返回查询结果集，失败返回长度为0的list集合
     */
    List<Map<String, Object>> selectByProject(@Param("year") String year, @Param("date") String date);

    /**
     * 地点统计
     *
     * @param year 通过年份查询
     * @param date 通过月份查询
     * @return 返回查询结果集，失败返回长度为0的list集合
     */
    List<Map<String, Object>> selectPosition(@Param("year") String year, @Param("date") String date);
}
