package com.overtime.account.service;

import com.overtime.account.mapper.AccountMoneyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/22.
 */
@Service
@Transactional
public class AccountMoneyService {
    @Autowired
    AccountMoneyMapper accountMoneyMapper;
    public List<Map<String,String>> getTatolDataByYear(String time){
        return accountMoneyMapper.getTotalDataByYear(time);
    }
    public List<Map<String,String>> getTotalDataByMonth(String time){
        return accountMoneyMapper.getTotalDataByMonth(time);
    }
    public List<Map<String,String>> getTotalData(){
        return accountMoneyMapper.getTotalData();
    }
    public List getTotalData1(){
        return accountMoneyMapper.getTotalData1();
    }
    public List getTatolDataByYear1(String time){
        return accountMoneyMapper.getTotalDataByYear1(time);
    }
    public List getTotalDataByMonth1(String time){
        return accountMoneyMapper.getTotalDataByMonth1(time);
    }
}
