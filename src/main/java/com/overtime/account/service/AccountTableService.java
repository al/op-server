package com.overtime.account.service;

import com.common.UserUtils;
import com.overtime.account.mapper.AccountTableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by zhong on 2018/8/28.
 */
@Service
@Transactional
public class AccountTableService {
    @Autowired
    AccountTableMapper accountTableMapper;
    public List getUser(int depno,String starttime,String endtime,String projectNumber,int[] positionid){
        if("".equals(starttime)){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            endtime = simpleDateFormat.format(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date); // 设置为当前时间
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1); // 设置为上一个月
            date = calendar.getTime();
            starttime =simpleDateFormat.format(date);
        }
        Map map = new HashMap();
        map.put("depno",depno);
        map.put("endtime",endtime);
        map.put("starttime",starttime);
        map.put("project_number",projectNumber);
        if(positionid.length>0){
            map.put("positionid",positionid);
        }

        return accountTableMapper.getUser(map);


    }
    public List getUserByExpenseId(int[] expenseIds){
        List list = accountTableMapper.getUserByExpenseId(expenseIds);
        return list;
    }

    public List getMoneyByExpenseId(int[] expenseIds){
        List<Map> userList = accountTableMapper.getUserByExpenseId(expenseIds);
        StringBuffer sb = new StringBuffer();
        sb.append("SUM(case c.name when '"+userList.get(0).get("name")+"' then fee else 0 end) AS '"+userList.get(0).get("name")+"'");
        for(int i=1;i<userList.size();i++){
            sb.append(",SUM(case c.name when '"+userList.get(i).get("name")+"' then fee else 0 end) AS '"+userList.get(i).get("name")+"'");
        }
        Map map = new HashMap();
        map.put("param",sb.toString());
        map.put("expenseIds",expenseIds);
        List list = accountTableMapper.getMoneyByExpenseId(map);
        return list;
    }
    public List getMoney(int depno,String starttime,String endtime,String projectNumber,int[] positionid){
        Map map = new HashMap();
        if("".equals(starttime)){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            endtime = simpleDateFormat.format(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date); // 设置为当前时间
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1); // 设置为上一个月
            date = calendar.getTime();
            starttime =simpleDateFormat.format(date);
        }
        if(depno==0){
            depno = UserUtils.getCurrentHr().getDepartmentId().intValue();
        }
        map.put("depno",depno);
        map.put("starttime",starttime);
        map.put("endtime",endtime);
        map.put("project_number",projectNumber);
        if(positionid.length>0){
            map.put("positionid",positionid);
        }
        List<Map> userList = accountTableMapper.getUser(map);
        StringBuffer sb = new StringBuffer();
        if(userList.size()>0){
            sb.append("SUM(case c.name when '"+userList.get(0).get("name")+"' then fee else 0 end) AS '"+userList.get(0).get("name")+"'");
            for(int i=1;i<userList.size();i++){
                sb.append(",SUM(case c.name when '"+userList.get(i).get("name")+"' then fee else 0 end) AS '"+userList.get(i).get("name")+"'");
            }

            map.put("param",sb.toString());
            return accountTableMapper.getMoney(map);
        }else{
            return null;
        }

    }
}
