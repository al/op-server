package com.overtime.account.service;

import com.overtime.account.mapper.AmountMapper;
import com.overtime.account.mapper.IndexCountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: 徐双翎
 * Date: 2020/1/18
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 * Description:
 */
@Service
@Transactional
public class AmountService {
    @Autowired
    private AmountMapper amountMapper;

    /**
     * 通过年份查询统计数据
     * 通过日期查询统计数据
     * 通过年份查询，则date填为date，反之year填为date
     *
     * @param year 需要查询的年份
     * @param date 需要查询的日期
     * @return
     */
    public Map<String, Object> countByYear(String year, String date) {
        //总result集合，存储最后结果
        Map<String, Object> result = new HashMap<>();
        //加班总天数、加班地点数、加班项目数、加班人员数查询
        List<Map<String, Object>> dayCount = amountMapper.selectCountDay(year, date);
        //工作日、节假日查询
        List<Map<String, Object>> holidayCount = amountMapper.selectWorkAndHoliday(year, date);
        dayCount.add(holidayCount.get(0));
        //将显示的数量统计信息添入返回的list中
        result.put("greyAmount", dayCount);
        //查询人员统计
        List<Map<String, Object>> personCount = amountMapper.selectByPerson(year, date);
        //查询报销统计
        List<Map<String, Object>> payCount = amountMapper.selectByPay(year, date);
        //查询项目统计
        List<Map<String, Object>> projectCount = amountMapper.selectByProject(year, date);
        //查询地点统计
        List<Map<String, Object>> positionCount = amountMapper.selectPosition(year, date);
        //将数据添加到返回集中
        result.put("personCount", personCount);
        result.put("payCount", payCount);
        result.put("projectCount", projectCount);
        result.put("positionCount", positionCount);
        //返回查询结果
        return result;
    }
}
