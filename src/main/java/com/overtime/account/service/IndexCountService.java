package com.overtime.account.service;

import com.overtime.account.mapper.IndexCountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 * Created by renxin on 2018/11/19 0019.
 */
@Service
@Transactional
public class IndexCountService {
    @Autowired
    private IndexCountMapper indexCountMapper;

    public List<Map<String,Object>> expenseCount(String month) throws Exception {
        Map<String, Object> map = putMap(month);
        return indexCountMapper.expenseCount(map);
    }

    public List<Map<String,Object>> applicationCount(String month) throws Exception{
        Map<String, Object> map = putMap(month);
        return indexCountMapper.applicationCount(map);
    }

    public List<Map<String,Object>> projectCount(String month) throws Exception{
        Map<String, Object> map = putMap(month);
        return indexCountMapper.projectCount(map);
    }

    private Map<String,Object> putMap(String month){
        Map map = new HashMap<String,Object>();
        if(!("".equals(month) || "null".equals(month))){
            map.put("month",month);
        }
        map.put("year", Calendar.getInstance().get(Calendar.YEAR));
        return map;
    }


}
