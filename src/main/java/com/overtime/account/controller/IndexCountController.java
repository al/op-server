package com.overtime.account.controller;

import com.overtime.account.service.IndexCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/11/19 0019.
 */
@RestController
public class IndexCountController {
    @Autowired
    private IndexCountService indexCountService;
    /**
     * 首页统计 加班天数
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/indexCount/applicationCount",method = RequestMethod.POST)
    public  List<Map<String,Object>> applicationCount(String month) throws Exception{

        return indexCountService.applicationCount(month);
    }

    /**
     * 首页查询  报销统计
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/indexCount/expenseCount",method = RequestMethod.POST)
    public List<Map<String,Object>> expenseCount(String month) throws Exception{
        return  indexCountService.expenseCount(month);
    }

    /**
     * 首页统计加班地点
     * @param time
     * @param month
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/indexCount/projectCount",method = RequestMethod.POST)
    public List<Map<String,Object>> projectCount(String month) throws Exception{
        return indexCountService.projectCount(month);
    }

}
