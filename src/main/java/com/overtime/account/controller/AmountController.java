package com.overtime.account.controller;

import com.overtime.account.service.AmountService;
import com.overtime.account.service.IndexCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: 徐双翎
 * Date: 2020/1/18
 * Time: 17:01
 * To change this template use File | Settings | File Templates.
 * Description: 数量统计
 */
@RestController
@RequestMapping("/amount")
public class AmountController {
    @Autowired
    private AmountService amountService;

    /**
     * 通过年份、日期查询统计情况
     *
     * @param year 需要查询的年份
     * @param date 需要查询的日期
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/amountCount")
    @ResponseBody
    public Map<String, Object> amountCount(String year, String date) throws Exception {
        System.out.println();
        return amountService.countByYear(year, date);
    }

}
