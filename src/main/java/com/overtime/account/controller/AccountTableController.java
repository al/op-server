package com.overtime.account.controller;

import com.overtime.account.service.AccountTableService;
import com.overtime.expense.bean.ExportExcel;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/28.
 */
@RestController
@RequestMapping("/accountTable")
public class AccountTableController {
    public static final float PXTOPT = 0.75f;
    @Autowired
    AccountTableService accountTableService;
    @RequestMapping("/getUser")
    public List getUser(int depno,String starttime,String endtime,String projectNumber,int[] positionid){
        return accountTableService.getUser(depno,starttime,endtime,projectNumber,positionid);
    }
    @RequestMapping("/getUserByExpenseId")
    public List getUserByExpenseId(int[] expenseIds){
        return accountTableService.getUserByExpenseId(expenseIds);
    }
    @RequestMapping("/getMoneyByExpenseId")
    public List getMoneyByExpenseId(int[] expenseIds){

        return accountTableService.getMoneyByExpenseId(expenseIds);
    }
    @RequestMapping("/getMoney")
    public List getMoney(int depno,String starttime,String endtime,String projectNumber,int[] positionid){
        return accountTableService.getMoney(depno,starttime,endtime,projectNumber,positionid);
    }
//    @RequestMapping("/download")
//    public void download(HttpServletResponse response,int depno,String starttime,String endtime) throws IOException {
//        ExportExcel exportExcel = new ExportExcel();
//
//        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        HSSFWorkbook wb = new HSSFWorkbook();
//        HSSFSheet sheet = wb.createSheet("line");
//
//        HSSFRow row = sheet.createRow(1);
//        row.setHeightInPoints(77 * PXTOPT);
//        List list = accountTableService.getUser(depno,starttime,endtime);//表头
//        List list1 = accountTableService.getMoney(depno,starttime,endtime);//数据
//
//        final String text = "日期\n\n\n        金额\n\n\n\n\n\n         人员";
//        HSSFCell cell = row.createCell(0);
//        HSSFCellStyle cellStyle = exportExcel.getCellFormat(wb);
//        int x1 = 61, y1 = 77;
//        int x2 = 144, y2 = 39;
//        /*int x3 = 144, y3 = 31;*/
//        int[] xys = {x1, y1, x2, y2};
//        exportExcel.drawLine(sheet, row, 1, 0, 144, 77, xys);
//        cell.setCellValue(text);
//
//        cell.setCellStyle(cellStyle);
//        for(int i=0;i<list.size();i++){
//            cell = row.createCell(i+1);
//            cell.setCellStyle(cellStyle);
//            cell.setCellValue((String) ((Map)list.get(i)).get("name"));
//        }
//        cell = row.createCell(list.size()+1);
//        cell.setCellStyle(cellStyle);
//        cell.setCellValue("总计");
//        for(int i=0;i<list1.size();i++){
//            row = sheet.createRow(i+2);
//            cell = row.createCell(0);
//            cell.setCellStyle(cellStyle);
//            cell.setCellValue((String) ((Map)list1.get(i)).get("theTime"));
//            for(int j=0;j<list.size();j++){
//                cell = row.createCell(j+1);
//                cell.setCellStyle(cellStyle);
//                cell.setCellValue((((Map)list1.get(i)).get((String) ((Map)list.get(j)).get("name"))).toString());
//            }
//            cell = row.createCell(list.size()+1);
//            cell.setCellStyle(cellStyle);
//            cell.setCellValue((((Map)list1.get(i)).get("theAll")).toString());
//        }
//        HSSFCellStyle titleStyle = wb.createCellStyle();        //标题样式
//        titleStyle.setAlignment(HorizontalAlignment.CENTER);
//        Font ztFont = wb.createFont();
//        ztFont.setItalic(false);                     // 设置字体为斜体字
//        ztFont.setFontHeightInPoints((short)16);    // 将字体大小设置为18px
//        ztFont.setFontName("宋体");             // 将“宋体”字体应用到当前单元格上
//        ztFont.setBold(true);
//        titleStyle.setFont(ztFont);
//        row=sheet.createRow(0);
//        cell=row.createCell(0);
//        sheet.addMergedRegion(new CellRangeAddress(0,0,0,list.size()+1));
//        cell.setCellStyle(titleStyle);
//        cell.setCellValue("加班报销统计表");
//
//        try {
////           FileOutputStream fos = new FileOutputStream("D:/line.xls");
//            wb.write(os);
//        } catch (Exception e) {
//
//
//        }
//        byte[] content = os.toByteArray();
//        InputStream is = new ByteArrayInputStream(content);
//        // 设置response参数，可以打开下载页面
//        response.reset();
//        response.setContentType("application/vnd.ms-excel;charset=utf-8");
//        response.setHeader("Content-Disposition",
//                "attachment;filename=" + new String(("报销金额表" + ".xls").getBytes(), "iso-8859-1"));
//        ServletOutputStream out = response.getOutputStream();
//        BufferedInputStream bis = null;
//        BufferedOutputStream bos = null;
//        try {
//            bis = new BufferedInputStream(is);
//            bos = new BufferedOutputStream(out);
//            byte[] buff = new byte[2048];
//            int bytesRead;
//            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
//                bos.write(buff, 0, bytesRead);
//            }
//        } catch (final IOException e) {
//            throw e;
//        } finally {
//            if (bis != null){
//                bis.close();}
//            if (bos != null){
//                bos.close();}
//        }
//
//    }
}
