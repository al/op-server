package com.overtime.account.controller;

import com.overtime.account.service.AccountMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/22.
 */
@RestController
@RequestMapping("/account")
public class AccountMoneyController {
    @Autowired
    AccountMoneyService accountMoneyService;

    private static Map<String,String> map4Map=new HashMap();
    static {
        map4Map.put("北京市","北京");
        map4Map.put("天津市","天津");
        map4Map.put("河北省","河北");
        map4Map.put("山西省","山西");
        map4Map.put("内蒙古自治区","内蒙古");
        map4Map.put("辽宁省","辽宁");
        map4Map.put("吉林省","吉林");
        map4Map.put("黑龙江省","黑龙江");
        map4Map.put("上海市","上海");
        map4Map.put("江苏省","江苏");
        map4Map.put("浙江省","浙江");
        map4Map.put("安徽省","安徽");
        map4Map.put("福建省","福建");
        map4Map.put("江西省","江西");
        map4Map.put("山东省","山东");
        map4Map.put("河南省","河南");
        map4Map.put("湖北省","湖北");
        map4Map.put("湖南省","湖南");
        map4Map.put("广东省","广东");
        map4Map.put("广西壮族自治区","广西");
        map4Map.put("海南省","海南");
        map4Map.put("重庆市","重庆");
        map4Map.put("四川省","四川");
        map4Map.put("贵州省","贵州");
        map4Map.put("云南省","云南");
        map4Map.put("西藏自治区","西藏");
        map4Map.put("陕西省","陕西");
        map4Map.put("甘肃省","甘肃");
        map4Map.put("青海省","青海");
        map4Map.put("宁夏回族自治区","宁夏");
        map4Map.put("新疆维吾尔自治区","新疆");
        map4Map.put("台湾省","台湾");
        map4Map.put("香港特别行政区","香港");
        map4Map.put("澳门特别行政区","澳门");
    }
    @RequestMapping("/getTotalData")
    public List getTotalData(String timeStr){
        if("-1".equals(timeStr)){
            List<Map<String,String>> list = accountMoneyService.getTotalData();
            for(Map<String,String> map : list){
                String name = map4Map.get(map.get("name"));
                map.put("name",name);
            }
            return list;
        }else if(timeStr.indexOf("-")==-1){
            List<Map<String,String>> list =accountMoneyService.getTatolDataByYear(timeStr);
            for(Map<String,String> map : list){
                String name = map4Map.get(map.get("name"));
                map.put("name",name);
            }
            return list;
        }else{
            List<Map<String,String>> list = accountMoneyService.getTotalDataByMonth(timeStr);
            for(Map<String,String> map : list){
                String name = map4Map.get(map.get("name"));
                map.put("name",name);
            }
            return list;
        }
    }

    @RequestMapping("/getTotalData1")
    public List getTotalData1(String timeStr){
        if("-1".equals(timeStr)){
            return accountMoneyService.getTotalData1();
        }else if(timeStr.indexOf("-")==-1){
            return accountMoneyService.getTatolDataByYear1(timeStr);
        }else{
            return accountMoneyService.getTotalDataByMonth1(timeStr);
        }
    }
}
