package com.overtime.project.mapper;


import com.overtime.project.bean.ProjectManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProjectMapper {
    List<ProjectManage> getAllPro();

    List<ProjectManage> getNewData(@Param("pro") ProjectManage pro);

    List<ProjectManage> getById(@Param("pro") ProjectManage pro);

    int addPro(@Param("pro") ProjectManage pro);

    int deleteProById(@Param("pids") String[] pids);

    int updateProById(@Param("pro") ProjectManage pro);

    List<ProjectManage> getIdPro();

    ProjectManage getProByName(String name);

    ProjectManage getProByNum(String number);

    int getTotal();

    List<ProjectManage> getProjectData(@Param(value = "depno") String depno);

    List<Map<String, Object>> loadDept(@Param(value = "grade") int grade);
}
