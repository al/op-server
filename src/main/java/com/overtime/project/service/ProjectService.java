package com.overtime.project.service;


import com.github.pagehelper.PageHelper;
import com.overtime.expense.bean.PageBean;
import com.overtime.project.bean.ProjectManage;
import com.overtime.project.mapper.ProjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ProjectService {
    @Autowired
    ProjectMapper projectMapper;

    public List<ProjectManage> getAllPro() {

        return projectMapper.getAllPro();
    }

    public int getTotal() {
        int total = projectMapper.getTotal();
        return total;
    }

    public List<ProjectManage> getById(ProjectManage projectManage) {

        return projectMapper.getById(projectManage);
    }

    public List<ProjectManage> getNewData(ProjectManage projectManage) {

        return projectMapper.getNewData(projectManage);
    }

    public List<ProjectManage> getIdPro() {
        return projectMapper.getIdPro();
    }

    public int addPro(ProjectManage projectManage) {
        if (projectMapper.getProByName(projectManage.getProject_name()) != null) {
            return -1;
        }
        if (projectMapper.getProByNum(projectManage.getProject_number()) != null) {
            return 0;
        }
        return projectMapper.addPro(projectManage);
    }

    public int updateProById(ProjectManage projectManage) {

        return projectMapper.updateProById(projectManage);
    }

    public boolean deleteProById(String pids) {
        String[] split = pids.split(",");
        return projectMapper.deleteProById(split) == split.length;
    }

    public List<ProjectManage> getProjectData(String depno) {
        return projectMapper.getProjectData(depno);
    }


    public List<Map<String, Object>> loadDept() {
        int grade = 2;
        return projectMapper.loadDept(grade);
    }
}
