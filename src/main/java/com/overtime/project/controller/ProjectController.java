package com.overtime.project.controller;




import com.base.bean.RespBean;
import com.overtime.project.bean.ProjectManage;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.overtime.project.service.ProjectService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class ProjectController {
    @Autowired
    ProjectService projectService;
    ProjectManage projectManage;
    @RequestMapping(value = "/project/manage", method = RequestMethod.POST)
   /* public Map<String, Object> proInfo(int currentPage,int pageSize){
        List<ProjectManage> list = projectService.getAllPro(currentPage,pageSize);
        Map<String, Object> map = new HashMap<String, Object>();
        int total =projectService.getTotal();
        map.put("list", list);
        map.put("number", total);
        return map;
    }*/
    public List<ProjectManage> proInfo() {

        return projectService.getAllPro();
    }
    @RequestMapping(value = "/project/manage/dialog", method = RequestMethod.GET)
    public List<ProjectManage> getIdPro() {

        return projectService.getIdPro();
    }

    @RequestMapping(value = "/project/manage/select", method = RequestMethod.POST)
    public List<ProjectManage> getNewData(ProjectManage projectManage) {

        return projectService.getNewData(projectManage);
    }

    @RequestMapping(value = "/pro/getById", method = RequestMethod.POST)
    public List<ProjectManage> getById(ProjectManage projectManage) {

        return projectService.getById(projectManage);
    }
    @RequestMapping(value = "/project/manage/pro", method = RequestMethod.POST)
    public RespBean addPro(ProjectManage projectManage) {
        int result = projectService.addPro(projectManage);
        if (result == 1) {
            return new RespBean("success", "添加成功!");
        } else if (result == -1) {
            return new RespBean("error", "项目名称重复，添加失败!");
        }else if (result == 0) {
            return new RespBean("error", "项目编号重复，添加失败!");
        }
        return new RespBean("error", "添加失败!");
    }
    @RequestMapping(value = "/project/manage/editpro", method = RequestMethod.POST)
    public RespBean updateProById(ProjectManage projectManage) {
        int result = projectService.updateProById(projectManage);
        if (result == 1) {
            return new RespBean("success", "更新成功!");
        }
        return new RespBean("error", "更新!");
    }
    @RequestMapping("/project/manage/del/{pids}")
    public RespBean deletePosById(@PathVariable String pids) {
        if (projectService.deleteProById(pids)) {
            return new RespBean("success", "删除成功!");
        }
        return new RespBean("error", "删除失败!");
    }

    @RequestMapping("/project/getProjectData")
    public List<ProjectManage> getProjectData(String depno){
        return projectService.getProjectData(depno);
    }

    @RequestMapping("/project/manage/deptid")
    public List<Map<String,Object>> loadDept() throws Exception{
        return projectService.loadDept();
    }


}
