package com.overtime.application.bean;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by renxin on 2018/8/16 0016.
 */
public class Application {
    //主键
    private Integer application_id;
    //审核级数ID
    private Integer audit_level_id;
    //申请人主键
    private long user_id;
    //项目编号主键
    private Integer project_id;
    //部门编号主键
    private Integer department_id;
    //申请人姓名
    private String user_name;
    //加班申请理由
    private String application_context;
    //加班申请类型
    private Integer application_type;
    //加班申请时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date application_time;
    //加班申请状态
    private Integer application_status;
    //加班申请审核时间
    private Date application_audit_time;
    //加班申请预计开始时间
    private Date application_start_time;
    //加班申请预计结束时间
    private Date application_end_time;
    //加班申请备注
    private String application_note;
    //当前审核顺序
    private Integer audit_level_now;
    //加班地点
    private String application_place;
    //
    private Long create_user;

    private Long depno;
    //时间统计,统计时间段内,有多少工作日,周末和节假日
    private String accout_time;

    public String getAccout_time() {
        return accout_time;
    }

    public void setAccout_time(String accout_time) {
        this.accout_time = accout_time;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public Long getDepno() {
        return depno;
    }

    public void setDepno(Long depno) {
        this.depno = depno;
    }

    public Long getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Long create_user) {
        this.create_user = create_user;
    }

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public Integer getAudit_level_id() {
        return audit_level_id;
    }

    public void setAudit_level_id(Integer audit_level_id) {
        this.audit_level_id = audit_level_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getApplication_context() {
        return application_context;
    }

    public void setApplication_context(String application_context) {
        this.application_context = application_context;
    }

    public Integer getApplication_type() {
        return application_type;
    }

    public void setApplication_type(Integer application_type) {
        this.application_type = application_type;
    }
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    public Date getApplication_time() {
        return application_time;
    }

    public void setApplication_time(Date application_time) {
        this.application_time = application_time;
    }

    public Integer getApplication_status() {
        return application_status;
    }

    public void setApplication_status(Integer application_status) {
        this.application_status = application_status;
    }

    public Date getApplication_audit_time() {
        return application_audit_time;
    }

    public void setApplication_audit_time(Date application_audit_time) {
        this.application_audit_time = application_audit_time;
    }

    public Date getApplication_start_time() {
        return application_start_time;
    }

    public void setApplication_start_time(Date application_start_time) {
        this.application_start_time = application_start_time;
    }

    public Date getApplication_end_time() {
        return application_end_time;
    }

    public void setApplication_end_time(Date application_end_time) {
        this.application_end_time = application_end_time;
    }

    public String getApplication_note() {
        return application_note;
    }

    public void setApplication_note(String application_note) {
        this.application_note = application_note;
    }

    public Integer getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Integer department_id) {
        this.department_id = department_id;
    }

    public Integer getAudit_level_now() {
        return audit_level_now;
    }

    public void setAudit_level_now(Integer audit_level_now) {
        this.audit_level_now = audit_level_now;
    }

    public String getApplication_place() {
        return application_place;
    }

    public void setApplication_place(String application_place) {
        this.application_place = application_place;
    }
}
