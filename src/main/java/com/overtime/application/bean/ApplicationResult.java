package com.overtime.application.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renxin on 2018/8/21 0021.
 */
public class ApplicationResult {
    //加班结果表主键
    private Integer result_id;
    //加班申请表主键
    private Integer application_id;
    //加班结果状态    1.未开始加班  2.加班中  3.加班结束
    private Integer result_status;
    //加班实际开始时间
    private Date result_start_time;
    //加班实际结果时间
    private Date result_end_time;
    //是否产生加班费   0未产生  1为已产生
    private Integer is_result_fee;

    private Integer result_time_day;

    private Long user_id;

    private String account_time;

    public String getAccount_time() {
        return account_time;
    }

    public void setAccount_time(String account_time) {
        this.account_time = account_time;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Integer getResult_time_day() {
        return result_time_day;
    }

    public void setResult_time_day(Integer result_time_day) {
        this.result_time_day = result_time_day;
    }

    public Integer getIs_result_fee() {
        return is_result_fee;
    }

    public void setIs_result_fee(Integer is_result_fee) {
        this.is_result_fee = is_result_fee;
    }

    public Integer getResult_id() {
        return result_id;
    }

    public void setResult_id(Integer result_id) {
        this.result_id = result_id;
    }

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public Integer getResult_status() {
        return result_status;
    }

    public void setResult_status(Integer result_status) {
        this.result_status = result_status;
    }

    public Date getResult_start_time() {
        return result_start_time;
    }

    public void setResult_start_time(Date result_start_time) {
        this.result_start_time = result_start_time;
    }

    public Date getResult_end_time() {
        return result_end_time;
    }

    public void setResult_end_time(Date result_end_time) {
        this.result_end_time = result_end_time;
    }
}
