package com.overtime.application.service;

import com.base.bean.RespBean;
import com.overtime.application.bean.Application;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/8/16 0016.
 */
public interface ApplicationService {
    /**
     * 分页查询
     * @param application
     * @param page_num
     * @param page_size
     * @param overtime_all_time
     * @return
     * @throws Exception
     */
    Map<String,Object> query(Application application,int page_num,int page_size, String overtime_all_time) throws Exception;

    /**
     * 查询部门列表
     * @return
     * @throws Exception
     */
    List<Map<String, Object>> queryDepartment() throws Exception;

    /**
     * 新增加班申请单
     * @param application
     * @param overtime_all_time
     * @param request
     * @return
     * @throws Exception
     */
    RespBean applicationSave(Application application, String overtime_all_time, HttpServletRequest request) throws Exception;

    /**
     * 查询用户下拉框
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> queryUser(int depno) throws Exception;

    /**
     * 删除操作
     * @param ids
     * @return
     * @throws Exception
     */
    RespBean delApplication(String[] ids)throws Exception;

    /**
     * 通过主键ID进行查询数据
     * @param application_id
     * @return
     */
    List<Map<String,Object>> queryById(String application_id) throws Exception;

    /**
     * 编辑保存方法
     * @param application
     * @param overtime_all_time
     * @return
     */
    RespBean applicationEdit(Application application, String overtime_all_time) throws Exception;

    /**
     * 查询项目列表
     * @return
     */
    List<Map<String,Object>> queryProject() throws Exception;
    List queryAL(int al);
}
