package com.overtime.application.service.impl;

import com.base.bean.RespBean;
import com.common.UserUtils;
import com.overtime.application.bean.Application;
import com.overtime.application.bean.ApplicationResult;
import com.overtime.application.mapper.ApplicationMapper;
import com.overtime.application.mapper.ApplicationResultMapper;
import com.overtime.application.service.ApplicationResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by renxin on 2018/8/21 0021.
 */
@Service("applicationResultService")
public class ApplicationResultServiceImpl implements ApplicationResultService {
    @Autowired
    private ApplicationResultMapper applicationResultMapper;
    @Autowired
    private ApplicationMapper applicationMapper;

    @Override
    public void saveApplicationResult(Integer application_id) throws Exception{
        ApplicationResult applicationResult = new ApplicationResult();

        applicationResult.setApplication_id(application_id);
        applicationResult.setResult_status(1);
        //设置实际加班时间  以后会调用接口接驳下班打卡时间来进行赋值
//        applicationResult.setResult_start_time(new Date());
//        applicationResult.setResult_end_time(new Date());
        //赋值初始未产生加班餐费和加班车费
        applicationResult.setIs_result_fee(0);
        applicationResultMapper.saveApplicaitonResult(applicationResult);
    }

    @Override
    public Map<String, Object> resultQuery(String user_name, String resultStatus, String messageSign, int page_num, int page_size ,Long user_id) throws Exception{
        Map<String, Object> map = new HashMap<>();
        page_num = (page_num-1)*page_size;
        Long depno = UserUtils.getCurrentHr().getDepartmentId();
        List<Map<String, Object>> list = applicationResultMapper.resultQuery(user_name, resultStatus, messageSign, page_num, page_size,user_id,depno);
        Integer total = applicationResultMapper.resultAllSizeQuery(user_name,resultStatus,messageSign,user_id,depno);
        map.put("pageData",list);
        map.put("total",total);
        return map ;
    }

    @Override
    public RespBean delApplicationResult(String[] ids) throws Exception {
        RespBean respBean = new RespBean();
        try {
            Map map = new HashMap();
            map.put("user_id",UserUtils.getCurrentHr().getId());
            map.put("depno",UserUtils.getCurrentHr().getDepartmentId());
            if(!("".equals(ids[0]))){
                for (int i = 0;i < ids.length;i++){
                    List<Application> list = applicationResultMapper.queryApplicationByResultId(Integer.parseInt(ids[i]));
                    if(list.get(0).getApplication_status()!=3){
                        applicationResultMapper.delApplicationResult(Integer.parseInt(ids[i]));
                        applicationMapper.delApplication(list.get(0).getApplication_id());
                    }
                }
            }
            respBean.setStatus("success");
            respBean.setMsg("删除成功,已审核记录不能删除");
        }catch (Exception e){
            respBean.setMsg("删除出错,请联系管理员");
            respBean.setStatus("error");
            e.printStackTrace();
        }
        return respBean;
    }

    @Override
    public List<Map> queryById(Integer application_id) {
        long user_id=UserUtils.getCurrentHr().getId();
        long depno=UserUtils.getCurrentHr().getDepartmentId();
        Map map = new HashMap();
        map.put("application_id",application_id);
        map.put("user_id",user_id);
        map.put("depno",depno);
        return applicationResultMapper.queryById(map);
    }

    @Override
    public RespBean applicationResultEdit(ApplicationResult applicationResult) {
        int workDays=0;
        int weekend = 0;
        int holiday=0;
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.setTime(applicationResult.getResult_start_time());
        calendar1.setTime(applicationResult.getResult_end_time());
        Date start = calendar.getTime();
        Date end = calendar1.getTime();
        while (start.getTime()<=end.getTime()){
            Map map = new HashMap();
            map.put("time",start);
            List<Map> list = applicationMapper.getTime(map);
            if(calendar.get(Calendar.DAY_OF_WEEK)!=7&&calendar.get(Calendar.DAY_OF_WEEK)!=1){
                if(list.size()==0){
                    workDays++;
                }else {
                    holiday++;
                }
            }else {
                if(list.size()>0){
                    if((int)list.get(0).get("type")==0)
                        holiday++;
                    else
                        workDays++;
                }else{
                    weekend++;
                }
            }
            calendar.add(Calendar.DAY_OF_MONTH,1);
            start = calendar.getTime();
        }
        String account_time = applicationResult.getAccount_time();
        int trueWorkday = Integer.parseInt(account_time.split(",")[0].substring(4));
        int trueWeekend = Integer.parseInt(account_time.split(",")[2].substring(3));
        int trueHoliday = Integer.parseInt(account_time.split(",")[1].substring(4));
        if(trueHoliday>holiday){
            return new RespBean("error","输入节假日天数大于实际节假日天数");
        }
        if(trueWeekend>weekend){
            return new RespBean("error","输入周末天数大于实际周末天数");
        }
        if(trueWorkday>workDays){
            return new RespBean("error","输入工作日天数大于实际工作日天数");
        }
        int i = applicationResultMapper.editApplication(applicationResult);
        int j = applicationResultMapper.applicationResultEdit(applicationResult);
        if(i==1&&j==1)
            return new RespBean("success","修改成功");
        else
            return new RespBean("error","修改失败");
    }

    @Override
    public List<ApplicationResult> getResult(ApplicationResult applicationResult){
        return applicationResultMapper.getResult(applicationResult);
    }

    @Override
    public ApplicationResult queryResultById(Integer appliation_id) {
        return applicationResultMapper.queryResultById(appliation_id);
    }
}
