package com.overtime.application.service;

import com.base.bean.RespBean;
import com.overtime.application.bean.ApplicationResult;

import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/8/21 0021.
 */
public interface ApplicationResultService {
    /**
     * 新增加班结果
     * @param application_id
     */
    void saveApplicationResult(Integer application_id) throws Exception;

    /**
     * 通过用户姓名、加班状态、是否产生事后信息进行模糊查询
     * @param user_name
     * @param resultStatus
     *@param messageSign
     * @param page_num
     * @param pageSize
     * @return Map<String,Object>
     */
    Map<String,Object> resultQuery(String user_name, String resultStatus, String messageSign, int page_num, int pageSize,Long user_id) throws Exception;

    /**
     * 删除加班申请单时级联删除加班结果表中的数据
     * @param ids
     * @throws Exception
     */
    RespBean delApplicationResult(String[] ids) throws Exception;

    List<Map> queryById(Integer application_id);
    RespBean applicationResultEdit(ApplicationResult applicationResult);
    List<ApplicationResult> getResult(ApplicationResult applicationResult);
    ApplicationResult queryResultById(Integer appliation_id);
}
