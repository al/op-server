package com.overtime.application.mapper;

import com.overtime.application.bean.Application;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/8/16 0016.
 */
public interface ApplicationMapper {
    /**
     * 查询申请单列表
     * @param application
     * @param pageNum
     *@param pageSize @return
     * @throws Exception
     */
    List<Map<String,Object>> query(@Param("application") Application application, @Param("page_num") int pageNum, @Param("page_size") int pageSize) throws Exception;

    /**
     * 查询部门列表
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> queryDepartment() throws Exception;

    /**
     * 新增加班申请单
     * @param application
     * @throws Exception
     */
    void applicationSave(Application application) throws Exception;

    /**
     * 查询用户下拉框
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> queryUser(@Param(value = "depno") int depno) throws  Exception;

    /**
     * 删除操作
     * @param id
     * @throws Exception
     */
    void delApplication(Integer id) throws Exception;

    /**
     * 通过主键id进行查询
     * @param map
     * @return
     */
    List<Map<String,Object>> queryById(Map map) throws Exception;

    /**
     * 编辑保存方法
     * @param application
     * @throws Exception
     */
    void applicationEdit(Application application)throws Exception;

    /**
     * 查询当前条件下的所有的申请单的数量
     * @param application
     * @return
     * @throws Exception
     */
    int queryTotalSize(@Param("application")Application application)throws Exception;

    /**
     * 查询项目列表
     * @return
     * @throws Exception
     */
    List<Map<String,Object>> queryProject() throws Exception;

    List queryAL(Map map);

    List getTime(Map map);

    List<Application> queryApplicationByUserIdTime(@Param("application") Application application);
}
