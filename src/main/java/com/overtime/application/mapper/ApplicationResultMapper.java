package com.overtime.application.mapper;

import com.overtime.application.bean.Application;
import com.overtime.application.bean.ApplicationResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/8/21 0021.
 */
public interface ApplicationResultMapper {
    /**
     * 新增一个加班申请单，新增加班结果的数据
     * @param applicationResult
     * @throws Exception
     */
    void saveApplicaitonResult(ApplicationResult applicationResult) throws Exception;

    /**
     * 通过用户姓名、加班状态、是否产生事后标志进行模糊查询
     * @param user_name
     * @param page_num
     * @param page_size
     * @param result_status
     * @param message_sign
     * @return Map<String,Object>
     */
    List<Map<String,Object>> resultQuery(@Param(value = "user_name") String user_name, @Param(value = "result_status") String result_status, @Param(value = "message_sign") String message_sign, @Param(value = "page_num") int page_num, @Param(value = "page_size") int page_size,@Param(value = "user_id") Long user_id,@Param(value = "depno")Long depno) throws Exception;

    /**
     * 分页查询的总数
     * @param user_name
     * @param result_status
     * @param message_sign
     * @return
     */
    Integer resultAllSizeQuery(@Param(value = "user_name") String user_name, @Param(value = "result_status") String result_status, @Param(value = "message_sign") String message_sign,@Param("user_id")Long user_id,@Param(value = "depno")Long depno) throws Exception;

    /**
     * 删除加班申请单时级联删除加班结果表中的数据
     * @param result_id
     */
    void delApplicationResult(@Param(value = "result_id") int result_id)throws Exception;

    List<Map> queryById(Map map);
    int applicationResultEdit(ApplicationResult applicationResult);
    int editApplication(ApplicationResult applicationResult);
    List<ApplicationResult> getResult(ApplicationResult applicationResult);
    ApplicationResult getKey(@Param(value = "application_id") int application_id);
    ApplicationResult queryResultById(Integer appliation_id);
    List<Application> queryApplicationByResultId(Integer result_id);
}
