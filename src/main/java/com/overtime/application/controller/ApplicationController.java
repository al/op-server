package com.overtime.application.controller;

import com.base.bean.RespBean;
import com.base.bean.User;
import com.common.UserUtils;
import com.overtime.application.bean.Application;
import com.overtime.application.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/8/16 0016.
 */
@RestController
public class ApplicationController {
    @Autowired
    private ApplicationService applicationService;

    //查询部门列表
    @RequestMapping(value = "/application/queryDepartment",method = RequestMethod.GET)
    public List<Map<String,Object>> queryDepartment() throws Exception{
        List<Map<String,Object>> list = applicationService.queryDepartment();
        return list;
    }
    //查询项目列表
    @RequestMapping(value = "/application/queryProject",method = RequestMethod.GET)
    public List<Map<String ,Object>> queryProject() throws Exception{
        List<Map<String ,Object>> list = applicationService.queryProject();
        return list;
    }
    //查询用户列表
    @RequestMapping(value = "/application/queryUser",method = RequestMethod.POST)
    public List<Map<String,Object>> queryUser(int depno) throws Exception{
        List<Map<String,Object>> list  = applicationService.queryUser(depno);
        return  list;
    }

    //查询加班申请单列表
    @RequestMapping(value = "/application/query",method = RequestMethod.POST)
    public Map<String,Object> query(String overtime_all_time, @RequestParam(value = "currentPage") int pageNum, int pageSize, Application application) throws Exception{
        //Map<String, String[]> map = request.getParameterMap();
        application.setUser_id(UserUtils.getCurrentHr().getId());
        application.setDepno(UserUtils.getCurrentHr().getDepartmentId());
        Map<String,Object> map =  applicationService.query(application,pageNum,pageSize,overtime_all_time);
        return map;
    }
    //新增加班申请单
    @RequestMapping(value = "/application/applicationSave",method = RequestMethod.POST)
    public RespBean applicationSave(String overtime_all_time, Application application, HttpServletRequest request) throws Exception{
        User user = UserUtils.getCurrentHr();
        application.setCreate_user(user.getId());
        application.setAudit_level_now(1);
        RespBean respBean = applicationService.applicationSave(application,overtime_all_time,request);
        return respBean;
    }

    //删除加班申请单
    @RequestMapping(value = "/application/delApplication",method = RequestMethod.POST)
    public RespBean delApplication(String[] ids) throws Exception{
        RespBean respBean = applicationService.delApplication(ids);
        return respBean;
    }

    //通过加班申请表的主键进行查询
    @RequestMapping(value = "/application/queryById",method = RequestMethod.POST)
    public List<Map<String,Object>> queryById(String application_id) throws Exception{
        List<Map<String,Object>> list = applicationService.queryById(application_id);
        return  list;
    }

    //编辑保存方法
    @RequestMapping(value = "application/applicationEdit",method = RequestMethod.POST)
    public RespBean applicationEdit(Application application,String overtime_all_time) throws Exception{

        RespBean respBean = applicationService.applicationEdit(application,overtime_all_time);
        return  respBean;
    }
    @RequestMapping("application/queryAL")
    public List queryAL(){
        int i=0;
        return applicationService.queryAL(i);
    }
}
