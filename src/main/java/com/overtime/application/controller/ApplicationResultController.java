package com.overtime.application.controller;

import com.base.bean.RespBean;
import com.base.bean.User;
import com.common.UserUtils;
import com.overtime.application.bean.ApplicationResult;
import com.overtime.application.service.ApplicationResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by renxin on 2018/8/21 0021.
 */
@RestController
public class ApplicationResultController {
    @Autowired
    private ApplicationResultService applicationResultService;

    //查询加班结果表
    @RequestMapping("applicationResult/query")
    public Map<String,Object> resultQuery(String user_name, String result_status, String message_sign,@RequestParam(value = "currentPage") int page_num,int pageSize) throws Exception{
        Long user_id = UserUtils.getCurrentHr().getId();
        Map<String, Object> map = applicationResultService.resultQuery(user_name, result_status, message_sign,page_num,pageSize,user_id);
        return map;
    }

    @RequestMapping("applicationResult/queryById")
    public List<Map> queryById(Integer application_id){
        return applicationResultService.queryById(application_id);
    }

    @RequestMapping("applicationResult/applicationResultEdit")
    public RespBean applicationResultEdit(ApplicationResult applicationResult,String result_start_time,String result_end_time) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(result_end_time);
        applicationResult.setResult_start_time(simpleDateFormat.parse(result_start_time));
        applicationResult.setResult_end_time(simpleDateFormat.parse(result_end_time));
        List list = applicationResultService.getResult(applicationResult);
        if(list.size()>0){
            return new RespBean("warning","当前时间已在其他项目加班");
        }
        return applicationResultService.applicationResultEdit(applicationResult);

    }

    @RequestMapping("applicationResult/delApplicationResult")
    public RespBean delApplicationResult(String[] ids) throws Exception{
        RespBean respBean = applicationResultService.delApplicationResult(ids);

        return respBean;
    }
}
