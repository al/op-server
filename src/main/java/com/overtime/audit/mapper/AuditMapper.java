package com.overtime.audit.mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/16.
 */
public interface AuditMapper {
    List<Map> getApplication(Map map);
    int audit(Map map);
    int audit1(Map map);
    int callBack(Map map);
    List getTotal(Map map);
    Map getApplicationById(Map map);
}
