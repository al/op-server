package com.overtime.audit.controller;

import com.base.bean.RespBean;
import com.base.bean.User;
import com.common.UserUtils;
import com.overtime.application.service.ApplicationService;
import com.overtime.audit.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/16.
 * 加班审核
 */
@RestController
@RequestMapping("/audit")
public class AuditController {
    @Autowired
    AuditService auditService;
    @RequestMapping("/getApplication")
    public List<Map> getApplication(Integer start,Integer pageSize,String name){
        User user = UserUtils.getCurrentHr();
        return auditService.getApplication(user.getId(),start,pageSize,user.getDepartmentId(),name);
    }
    @RequestMapping("/audit")
    public RespBean audit(String audit_level_now,String application_id,String audit_level_id,String application_note){
        User user = UserUtils.getCurrentHr();
        Map map = new HashMap();
        int i = Integer.parseInt(audit_level_now)+1;
        map.put("audit_level_now",i);
        map.put("application_id",application_id);
        map.put("audit_level_id",audit_level_id);
        if("-1".equals(application_note)||application_note.isEmpty()){
            application_note="通过";
        }
        map.put("application_note","审核人:"+user.getName()+",审核意见:"+application_note+";");
        if(auditService.audit(map)==1){
            return new RespBean("success","已审核");
        }else{
            return new RespBean("error","审核失败");
        }
    }
    @RequestMapping("/auditAll")
    public RespBean auditAll(int[] application_ids) throws Exception {
        int i = auditService.auditAll(application_ids);
        if(i==application_ids.length){
            return new RespBean("success","审核成功");
        }else{
            return new RespBean("error","审核失败");
        }
    }
    @RequestMapping("/callBackAll")
    public RespBean callBackAll(int[] application_ids) throws Exception {
        int i = auditService.callBackAll(application_ids);
        if(i==application_ids.length){
            return new RespBean("success","已打回");
        }else{
            return new RespBean("error","打回失败");
        }
    }
    @RequestMapping("/callBack")
    public RespBean callBack(String application_id,String application_note){
        User user = UserUtils.getCurrentHr();
        if("-1".equals(application_note)){
            application_note="未说明";
        }
        Map map = new HashMap();
        map.put("application_id",application_id);
        map.put("application_note","审核人:"+user.getName()+",打回原因:"+application_note);
        if(auditService.callBack(map)==1){
            return new RespBean("success","已打回");
        }else{
            return new RespBean("error","打回失败");
        }
    }
    @RequestMapping("/getTotal")
    public List getTotal(String name){
        User user = UserUtils.getCurrentHr();
        return auditService.getTotal(user.getId(),name,user.getDepartmentId());
    }
}
