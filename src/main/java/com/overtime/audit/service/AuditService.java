package com.overtime.audit.service;

import com.common.UserUtils;
import com.overtime.application.mapper.ApplicationMapper;
import com.overtime.audit.mapper.AuditMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/16.
 */
@Service
@Transactional
public class AuditService {
    @Autowired
    AuditMapper auditMapper;
    @Autowired
    ApplicationMapper applicationMapper;
    public List<Map> getApplication(long userid,Integer start,Integer pageSize,long departmentId,String name){
        Map map = new HashMap();
        map.put("start",start);
        map.put("pageSize",pageSize);
        map.put("userid",userid);
        map.put("departmentId",departmentId);
        map.put("name",name);
        return auditMapper.getApplication(map);
    }

    public int audit(Map map){
        int i = auditMapper.audit(map);
        Map map1 = auditMapper.getApplicationById(map);
        if(((Integer)map1.get("audit_level_now"))>(Integer)map1.get("audit_level")){
            map.put("status",3);
        }else{
            map.put("status",2);
        }
        int j = auditMapper.audit1(map);


        if(j==1){
            return j;
        }else{
            return i;
        }

    }

    public int auditAll(int[] application_ids) throws Exception {
        int sum = 0;
        for(int i=0;i<application_ids.length;i++){
            long user_id= UserUtils.getCurrentHr().getId();
            long depno = UserUtils.getCurrentHr().getDepartmentId();
            Map map = new HashMap();
            map.put("id",application_ids[i]);
            map.put("user_id",user_id);
            map.put("depno",depno);
            List<Map<String, Object>> list = applicationMapper.queryById(map);
            Map map1 = new HashMap();
            map1.put("application_note","审核人:"+UserUtils.getCurrentHr().getName()+",审核意见:通过;");
            map1.put("audit_level_now",(Integer)list.get(0).get("audit_level_now")+1);
            map1.put("application_id",list.get(0).get("application_id"));
            map1.put("audit_level_id",list.get(0).get("audit_level_id"));
            sum+=audit(map1);
        }
        return sum;
    }

    public int callBackAll(int[] application_ids){
        int sum=0;
        for(int i=0;i<application_ids.length;i++){
            Map map = new HashMap();
            map.put("application_id",application_ids[i]);
            map.put("application_note","审核人:"+UserUtils.getCurrentHr().getName()+",打回原因:未说明");
            sum += callBack(map);
        }
        return sum;
    }

    public int callBack(Map map){
        return auditMapper.callBack(map);
    }
    public List getTotal(long userid,String name,long departmentId){
        Map map = new HashMap();
        map.put("userid",userid);
        map.put("name",name);
        map.put("departmentId",departmentId);
        return auditMapper.getTotal(map);
    }
}
