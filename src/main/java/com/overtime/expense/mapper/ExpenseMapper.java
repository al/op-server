package com.overtime.expense.mapper;

import com.base.bean.Position;
import com.base.bean.User;
import com.overtime.expense.bean.Chart;
import com.overtime.expense.bean.Expense;
import com.overtime.expense.bean.ExpensePo;
import com.overtime.expense.bean.ProjectBean;
import com.overtime.project.bean.ProjectManage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ExpenseMapper {
    List<Expense> getTableColumn(@Param("exp") Expense exp);

    List<Chart> getChart(Map map);

    List<Chart> getPie(Map map);

    List<Expense> getAllInfo(@Param("param") Map<String, Object> param);

    List<Expense> getAppliId();

    List<Expense> getAppliId1();

    List<Expense> getAduName();

    List<Expense> getExStu();

    List<ProjectBean> getProId();

    List<ProjectManage> getDeptIdPro();

    List<Expense> getExType();

    List<Expense> getUserData();

    List<Map<String, Object>> getNewData(Map map);

    int selectBeforeUpdate1(@Param("exp") Expense exp);

    int selectBeforeUpdate2(@Param("exp") Expense exp);

    int selectBeforeUpdate3(@Param("exp") Expense exp);

    int selectBeforeUpdate4(@Param("exp") Expense exp);

    int addExpenseFee1(@Param("eat_money") Double eat_money, @Param("id") int id);

    int addExpenseFee2(@Param("traffic_money") Double traffic_money, @Param("id") int id);

    int addExpenseFee3(@Param("extra_money") Double extra_money, @Param("id") int id);

    int addExpenseFee4(@Param("else_money") Double else_money, @Param("id") int id);

    int updateExpenseFee1(@Param("eat_money") Double eat_money, @Param("exp") Expense exp);

    int updateExpenseFee2(@Param("traffic_money") Double traffic_money, @Param("exp") Expense exp);

    int updateExpenseFee3(@Param("extra_money") Double extra_money, @Param("exp") Expense exp);

    int updateExpenseFee4(@Param("else_money") Double else_money, @Param("exp") Expense exp);

    List<Map<String, Object>> getDetailTable(Map map);

    List<Map<String, Object>> getDetailTableByExpenseIds(int[] expense_ids);
    List<Expense> getExpenseById(@Param("exp") Expense exp);

    List<Expense> getExUserName(@Param("exp") Expense exp);

    List<Position> getPosition();

    int getUserId(@Param("username") String userName);

    int addExpense(@Param("exp") ExpensePo exp);

    int updateExById(@Param("exp") Expense exp);

    int auditById(@Param("exp") Expense exp, @Param("username") String username);

    int deleteExById(@Param("pids") String[] pids);

    int deleteExFeeById(@Param("pids") String[] pids);

    int deleteExpenseFee1(@Param("exp") Expense exp);

    int deleteExpenseFee2(@Param("exp") Expense exp);

    int deleteExpenseFee3(@Param("exp") Expense exp);

    int deleteExpenseFee4(@Param("exp") Expense exp);

    int getPersonnalCount(@Param("exp") Expense exp);

    int countExpense();

    int getProTotal();

    int audit(Map map);

    int callback(Map map);

    List<Map> getProjectDataByTime(Map map);

    List<Map> getProjectDataByExpenseId(int[] expense_ids);
    List isAuditor(Long user_id);

    int auditApplication(Map map);
}
