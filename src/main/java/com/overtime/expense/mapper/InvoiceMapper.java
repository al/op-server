package com.overtime.expense.mapper;

import com.overtime.expense.bean.Invoice;

import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/20.
 */
public interface InvoiceMapper {
    List<Invoice> getAllInvoice(long userid);
    int updateInvoice(Invoice invoice);
    int deleteInvoice(String invoice_id);
    int insertInvoice(Invoice invoice);
    int insertPicture(Map map);
    List<Invoice> getInvoiceByName(Map map);
    List<Invoice> getInvoice(Map map);
}
