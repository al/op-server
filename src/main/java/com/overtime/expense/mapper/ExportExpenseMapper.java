package com.overtime.expense.mapper;

import com.overtime.expense.bean.ExportExpenseBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExportExpenseMapper {

    public List<ExportExpenseBean> queryExportExpenseList(@Param("ids") String[] ids);
}
