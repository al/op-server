package com.overtime.expense.controller;

import com.base.bean.User;
import com.common.UserUtils;
import com.overtime.expense.bean.ExportExpenseBean;
import com.overtime.expense.bean.ResultBean;
import com.overtime.expense.export.ExportExcelUtil;
import com.overtime.expense.service.ExportExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 导出报销单excel
 */

@RestController
public class ExportExpenseController {

    @Autowired
    ExportExpenseService exportExpenseService;

    @RequestMapping(value = "exportExpenseExcel", method = RequestMethod.GET)
    public ResultBean exportExpenseExcel(String expense_ids, String cause, HttpServletResponse response) throws Exception {
        ResultBean resultBean = new ResultBean();
        if(StringUtils.isEmpty(expense_ids)) {
            resultBean.setMsg("导出报销单为空！");
            resultBean.setStatus("0");
            return resultBean;
        }
        if(StringUtils.isEmpty(cause)) {
            resultBean.setMsg("导出报销单事由为空！");
            resultBean.setStatus("0");
            return resultBean;
        }
        String[] splitIds = expense_ids.split(",");
        List<ExportExpenseBean> exportExpenseBeans = exportExpenseService.queryExportExpenseList(splitIds);
        if(exportExpenseBeans.size() == 0) {
            resultBean.setMsg("查询报销单为空！");
            resultBean.setStatus("0");
            return resultBean;
        }
        ExportExpenseBean exportExpenseBean = exportExpenseBeans.get(0);
        String departName = exportExpenseBean.getDepartName();
        String projectName = exportExpenseBean.getProjectName();
        String projectNumber = exportExpenseBean.getProjectNumber();
        Map<String, List<ExportExpenseBean>> listMap = exportExpenseService.parseExportByDate(exportExpenseBeans);
        User currentHr = UserUtils.getCurrentHr();
        String name = currentHr.getName();
        ExportExcelUtil.exportExcel(listMap, response, departName, projectName, projectNumber, cause, name);
        return null;
    }
}
