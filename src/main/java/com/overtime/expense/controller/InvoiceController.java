package com.overtime.expense.controller;

import com.base.bean.RespBean;
import com.base.bean.User;
import com.common.FTPUtil;
import com.common.OPConst;
import com.common.UserUtils;
import com.overtime.expense.bean.Invoice;
import com.overtime.expense.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/20.
 */
@RestController
@RequestMapping("/invoice")
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    Environment env;
    @RequestMapping("/getAllInvoice")
    public List<Invoice> getAllInvoice(){
        User user = UserUtils.getCurrentHr();
        return invoiceService.getAllInvoice(user.getId());
    }
    @RequestMapping("/getInvoice")
    public List<Invoice> getInvoice(Integer start,Integer pageSize){
        User user = UserUtils.getCurrentHr();
        return invoiceService.getInvoice(user.getId(),start,pageSize);
    }

    @RequestMapping("/deleteInvoice")
    public RespBean deleteInvoice(String invoice_id){
        if(invoiceService.deleteInvoice(invoice_id)==1){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }
    @RequestMapping("/updateInvoice")
    public RespBean updateInvoice(Invoice invoice){
        if(invoiceService.updateInvoice(invoice)==1){
            return new RespBean("success","更新成功");
        }else{
            return new RespBean("error","更新失败");
        }
    }
    @RequestMapping("/insertInvoice")
    public RespBean insertInvoice(Invoice invoice){
        User user = UserUtils.getCurrentHr();
        invoice.setInvoice_upload_name(String.valueOf(user.getId()));
        invoice.setInvoice_picture(OPConst.DEFAULT_INVOICE.getValue());
        if(invoiceService.insertInvoice(invoice)==1){
            return new RespBean("success","新增成功");
        }else{
            return new RespBean("error","新增失败");
        }
    }
    @RequestMapping("/upload")
    public RespBean uploadHead(MultipartFile file, Long id) throws Exception {
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        if(!path.exists()) {
            path = new File("");
        }
        File upload = new File(path.getAbsolutePath(),"static/img/upload/");
        if(!upload.exists()) {
            upload.mkdirs();
        }
        String fileName = file.getOriginalFilename();
        fileName = id+"_"+System.currentTimeMillis()+fileName;
//        String storePath = upload.getAbsolutePath() + "/" + fileName;
//        File store = new File(storePath);
//        store.createNewFile();
//        try (FileOutputStream fos = new FileOutputStream(store)) {
//            FileCopyUtils.copy(file.getInputStream(), fos);
//            if (fos != null) {
//                fos.close();
//            }
//        }

        String urlPath = OPConst.HEAD_DOMAIN.getValue() + env.getProperty("ftp.rootPath") + "img/" + fileName;
        FTPUtil.getInstance(env).uploadFile(env.getProperty("ftp.rootPath") + "img/" , fileName, file.getInputStream());
        User user = UserUtils.getCurrentHr();
        Map map = new HashMap();
        map.put("invoice_picture",urlPath);
        map.put("invoice_id",id);
        map.put("invoice_uplaod_name",user.getId());
        if(invoiceService.insertPicture(map)==1){
            return new RespBean("success","上传成功",urlPath);
        }else{
            return new RespBean("error","上传失败");
        }
    }
    @RequestMapping("/getInvoiceByName")
    public List<Invoice> getInvoiceByName(String name){
        Map map = new HashMap();
        User user = UserUtils.getCurrentHr();
        map.put("invoice_name",name);
        map.put("invoice_upload_name",user.getId());
        return invoiceService.getInvoiceByName(map);
    }

    @RequestMapping("/deleteInvoices")
    public RespBean deleteInvoices(String ids){
        int sum = 0;
        for(String str:ids.split(",")){
            sum+=invoiceService.deleteInvoice(str);
        }
        int j = ids.split(",").length;
        if(sum==ids.split(",").length){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }
}
