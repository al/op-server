package com.overtime.expense.bean;

import java.math.BigDecimal;

public class ExpenseFeeBean {

    private Integer id;
    private Integer expense_id;
    private Integer expense_type;
    private BigDecimal fee;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(Integer expense_id) {
        this.expense_id = expense_id;
    }

    public Integer getExpense_type() {
        return expense_type;
    }

    public void setExpense_type(Integer expense_type) {
        this.expense_type = expense_type;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

}
