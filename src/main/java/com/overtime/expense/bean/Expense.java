package com.overtime.expense.bean;

import java.util.Date;

public class Expense {
    private  int exid;
    private String exname;
    private Double fee;
    private Double eatMoney;
    private Double trafficMoney;
    private Double extraMoney;
    private Double elseMoney;
    private String audstatu;
    private int audit_status;
    private Date application_date;
    private int date_type;

    public Date getApplication_date() {
        return application_date;
    }

    public void setApplication_date(Date application_date) {
        this.application_date = application_date;
    }

    public int getDate_type() {
        return date_type;
    }

    public void setDate_type(int date_type) {
        this.date_type = date_type;
    }

    public int getAudit_status() {
        return audit_status;
    }

    public void setAudit_status(int audit_status) {
        this.audit_status = audit_status;
    }

    public String getAudstatu() {
        return audstatu;
    }

    public void setAudstatu(String audstatu) {
        this.audstatu = audstatu;
    }

    public Double getEatMoney() {
        return eatMoney;
    }

    public void setEatMoney(Double eatMoney) {
        this.eatMoney = eatMoney;
    }

    public Double getTrafficMoney() {
        return trafficMoney;
    }

    public void setTrafficMoney(Double trafficMoney) {
        this.trafficMoney = trafficMoney;
    }

    public Double getExtraMoney() {
        return extraMoney;
    }

    public void setExtraMoney(Double extraMoney) {
        this.extraMoney = extraMoney;
    }

    public Double getElseMoney() {
        return elseMoney;
    }

    public void setElseMoney(Double elseMoney) {
        this.elseMoney = elseMoney;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public int getExid() {
        return exid;
    }

    public void setExid(int exid) {
        this.exid = exid;
    }

    public String getExname() {
        return exname;
    }

    public void setExname(String exname) {
        this.exname = exname;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    private int expense_id;
    private int application_id;
    private int project_id;
    private int expense_type;
    private double money;
    private String auditor;
    private String audit_comments;
    private int expense_status;
    private String statu;
    private String type;
    private String user_name;
    private  int user_id;
    private String user_relate;
    private int aaa102;
    private String aaa103;
    private String project_name;
    private  int total;
    private String lable;
    private int depno;
    private String projectNumber;
    private  String project_number;
    private  String create_user;
    private Date starttime;
    private Date endtime;

    public String getUser_relate() {
        return user_relate;
    }

    public void setUser_relate(String user_relate) {
        this.user_relate = user_relate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getExpense_cause() {
        return expense_cause;
    }

    public void setExpense_cause(String expense_cause) {
        this.expense_cause = expense_cause;
    }

    private String username;
    private String expense_cause;

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getProject_number() {
        return project_number;
    }

    public void setProject_number(String project_number) {
        this.project_number = project_number;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }



    public int getDepno() {
        return depno;
    }

    public void setDepno(int depno) {
        this.depno = depno;
    }
    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public int getAaa102() {
        return aaa102;
    }

    public void setAaa102(int aaa102) {
        this.aaa102 = aaa102;
    }

    public String getAaa103() {
        return aaa103;
    }

    public void setAaa103(String aaa103) {
        this.aaa103 = aaa103;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(int expense_id) {
        this.expense_id = expense_id;
    }

    public int getApplication_id() {
        return application_id;
    }

    public void setApplication_id(int application_id) {
        this.application_id = application_id;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getExpense_type() {
        return expense_type;
    }

    public void setExpense_type(int expense_type) {
        this.expense_type = expense_type;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getAudit_comments() {
        return audit_comments;
    }

    public void setAudit_comments(String audit_comments) {
        this.audit_comments = audit_comments;
    }

    public int getExpense_status() {
        return expense_status;
    }

    public void setExpense_status(int expense_status) {
        this.expense_status = expense_status;
    }
}
