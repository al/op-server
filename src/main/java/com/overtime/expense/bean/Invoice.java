package com.overtime.expense.bean;

/**
 * Created by zhong on 2018/8/20.
 */
public class Invoice {
    private Integer invoice_id;
    private String invoice_upload_name;
    private String invoice_number;
    private String invoice_name;
    private String invoice_purpose;
    private String invoice_address;
    private String invoice_telephone;
    private String invoice_receive_name;
    private String invoice_pay_name;
    private String invoice_time;
    private String invoice_money;
    private String invoice_status;
    private String invoice_picture;

    public Integer getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(Integer invoice_id) {
        this.invoice_id = invoice_id;
    }

    public String getInvoice_upload_name() {
        return invoice_upload_name;
    }

    public void setInvoice_upload_name(String invoice_upload_name) {
        this.invoice_upload_name = invoice_upload_name;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public String getInvoice_name() {
        return invoice_name;
    }

    public void setInvoice_name(String invoice_name) {
        this.invoice_name = invoice_name;
    }

    public String getInvoice_purpose() {
        return invoice_purpose;
    }

    public void setInvoice_purpose(String invoice_purpose) {
        this.invoice_purpose = invoice_purpose;
    }

    public String getInvoice_address() {
        return invoice_address;
    }

    public void setInvoice_address(String invoice_address) {
        this.invoice_address = invoice_address;
    }

    public String getInvoice_telephone() {
        return invoice_telephone;
    }

    public void setInvoice_telephone(String invoice_telephone) {
        this.invoice_telephone = invoice_telephone;
    }

    public String getInvoice_receive_name() {
        return invoice_receive_name;
    }

    public void setInvoice_receive_name(String invoice_receive_name) {
        this.invoice_receive_name = invoice_receive_name;
    }

    public String getInvoice_pay_name() {
        return invoice_pay_name;
    }

    public void setInvoice_pay_name(String invoice_pay_name) {
        this.invoice_pay_name = invoice_pay_name;
    }

    public String getInvoice_time() {
        return invoice_time;
    }

    public void setInvoice_time(String invoice_time) {
        this.invoice_time = invoice_time;
    }

    public String getInvoice_money() {
        return invoice_money;
    }

    public void setInvoice_money(String invoice_money) {
        this.invoice_money = invoice_money;
    }

    public String getInvoice_status() {
        return invoice_status;
    }

    public void setInvoice_status(String invoice_status) {
        this.invoice_status = invoice_status;
    }

    public String getInvoice_picture() {
        return invoice_picture;
    }

    public void setInvoice_picture(String invoice_picture) {
        this.invoice_picture = invoice_picture;
    }
}
