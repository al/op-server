package com.overtime.expense.bean;

/**
 * @ Author     ：x.
 * @ Date       ：Created in 16:56 2018/8/22
 * @ Description：
 * @ Modified By：
 * @Version: $
 */
public class Chart {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
