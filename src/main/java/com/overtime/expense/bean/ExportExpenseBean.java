package com.overtime.expense.bean;

import java.util.List;

public class ExportExpenseBean {

    private Integer expense_id;
    private Integer application_id;
    private String departName;
    private String projectName;
    private String projectNumber;
    private String cause;
    private String user_id;
    private String userName;
    private String application_place;
    private String application_date;
    private String date_Type;
    private List<ExpenseFeeBean> expenseFeeList;

    public Integer getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(Integer expense_id) {
        this.expense_id = expense_id;
    }

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getApplication_date() {
        return application_date;
    }

    public void setApplication_date(String application_date) {
        this.application_date = application_date;
    }

    public String getDate_Type() {
        return date_Type;
    }

    public void setDate_Type(String date_Type) {
        this.date_Type = date_Type;
    }

    public List<ExpenseFeeBean> getExpenseFeeList() {
        return expenseFeeList;
    }

    public void setExpenseFeeList(List<ExpenseFeeBean> expenseFeeList) {
        this.expenseFeeList = expenseFeeList;
    }

    public String getApplication_place() {
        return application_place;
    }

    public void setApplication_place(String application_place) {
        this.application_place = application_place;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
