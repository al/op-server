package com.overtime.expense.bean;

public class ResultBean {
    private String status;
    private String msg;
    private String result;

    public ResultBean() {
    }

    public ResultBean(String status, String msg) {

        this.status = status;
        this.msg = msg;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
