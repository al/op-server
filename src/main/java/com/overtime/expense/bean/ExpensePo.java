package com.overtime.expense.bean;

import java.util.Date;

public class ExpensePo {

    private Integer expense_id;
    private Integer application_id;
    private Integer project_id;
    private Integer auditor;
    private String audit_comments;
    private String audit_status;
    private String expense_cause;
    private Integer expense_status;
    private String create_time;
    private String update_time;
    private String user_relate;
    private Integer create_user;
    private Date application_date;
    private int date_type;

    public Date getApplication_date() {
        return application_date;
    }

    public void setApplication_date(Date application_date) {
        this.application_date = application_date;
    }

    public int getDate_type() {
        return date_type;
    }

    public void setDate_type(int date_type) {
        this.date_type = date_type;
    }

    public Integer getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(Integer expense_id) {
        this.expense_id = expense_id;
    }

    public Integer getApplication_id() {
        return application_id;
    }

    public void setApplication_id(Integer application_id) {
        this.application_id = application_id;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public Integer getAuditor() {
        return auditor;
    }

    public void setAuditor(Integer auditor) {
        this.auditor = auditor;
    }

    public String getAudit_comments() {
        return audit_comments;
    }

    public void setAudit_comments(String audit_comments) {
        this.audit_comments = audit_comments;
    }

    public String getAudit_status() {
        return audit_status;
    }

    public void setAudit_status(String audit_status) {
        this.audit_status = audit_status;
    }

    public String getExpense_cause() {
        return expense_cause;
    }

    public void setExpense_cause(String expense_cause) {
        this.expense_cause = expense_cause;
    }

    public Integer getExpense_status() {
        return expense_status;
    }

    public void setExpense_status(Integer expense_status) {
        this.expense_status = expense_status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getUser_relate() {
        return user_relate;
    }

    public void setUser_relate(String user_relate) {
        this.user_relate = user_relate;
    }

    public Integer getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Integer create_user) {
        this.create_user = create_user;
    }
}
