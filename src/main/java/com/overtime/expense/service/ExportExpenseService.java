package com.overtime.expense.service;

import com.overtime.expense.bean.ExportExpenseBean;
import com.overtime.expense.mapper.ExportExpenseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ExportExpenseService {

    @Autowired
    ExportExpenseMapper exportExpenseMapper;

    public List<ExportExpenseBean> queryExportExpenseList(String[] ids) {
        return exportExpenseMapper.queryExportExpenseList(ids);
    }

    public Map<String, List<ExportExpenseBean>> parseExportByDate(List<ExportExpenseBean> list) {
        //项目费用明细单
        Map<String, List<ExportExpenseBean>> dateMaps = new TreeMap<>();
        for (int i = 0, size = list.size(); i < size; i++) {
            ExportExpenseBean exportExpenseBean = list.get(i);
            String application_date = exportExpenseBean.getApplication_date();
            dateMaps.put(application_date, new ArrayList<>());
        }
        Set<String> strings = dateMaps.keySet();
        Iterator<String> iterator = strings.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            List<ExportExpenseBean> exportExpenseBeans = dateMaps.get(next);
            for (int j = 0, len = list.size(); j < len; j++) {
                ExportExpenseBean eb = list.get(j);
                if(next.equals(eb.getApplication_date())) {
                    exportExpenseBeans.add(eb);
                }
            }
        }
        return dateMaps;
    }

}
