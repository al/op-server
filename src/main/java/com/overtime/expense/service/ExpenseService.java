package com.overtime.expense.service;

import com.base.bean.Position;
import com.base.bean.RespBean;
import com.base.bean.User;
import com.common.UserUtils;
import com.github.pagehelper.PageHelper;
import com.overtime.application.mapper.ApplicationMapper;
import com.overtime.expense.bean.*;
import com.overtime.expense.mapper.ExpenseMapper;
import com.overtime.project.bean.ProjectManage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.common.UserUtils.getCurrentHr;

@Service
@Transactional
public class ExpenseService {
    @Autowired
    ExpenseMapper expenseMapper;

    @Autowired
    ApplicationMapper applicationMapper;

    /*
     * 分页功能(集成mybatis的分页插件pageHelper实现)
     * */
    public List<Expense> getTableColumn(Expense expense) {
        return expenseMapper.getTableColumn(expense);
    }

    public int getTotal() {
        int total = expenseMapper.countExpense();
        return total;
    }

    public int getProTotal() {
        int total = expenseMapper.getProTotal();
        return total;
    }

    public List<Map> getTime(Map map){
        return applicationMapper.getTime(map);
    }

    public List<Chart> getChart(String mouth, String year, String user_id) {
        Map map = new HashMap();
        map.put("mouth", mouth);
        map.put("year", year);
        if ("".equals(user_id)) {
            map.put("userid", UserUtils.getCurrentHr().getId());
        } else {
            map.put("userid", user_id);
        }

        return expenseMapper.getChart(map);
    }

    public List<Chart> getPie(String mouth, String year, String user_id) {
        Map map = new HashMap();
        map.put("mouth", mouth);
        map.put("year", year);
        if (user_id == "") {
            map.put("user_id", UserUtils.getCurrentHr().getId());
        } else {
            map.put("user_id", user_id);
        }
        return expenseMapper.getPie(map);
    }

    /*public List<Expense> getAllInfo(int currentPage,int pageSize){
        PageHelper.startPage(currentPage, pageSize);
        //全部报销单
        List<Expense> allExpense =expenseMapper.getAllInfo();
        //总记录数
        int total = expenseMapper.countExpense();

        PageBean<Expense>pageData = new PageBean<>(currentPage, pageSize, total);
        pageData.setItems(allExpense);
        return pageData.getItems();
    }*/
    public List<Expense> getAllInfo(Map<String, Object> param) {
        User user = UserUtils.getCurrentHr();
        param.put("id", user.getId());
        param.put("departmentId", user.getDepartmentId());
        List<Expense> allInfo = expenseMapper.getAllInfo(param);
        return allInfo;
    }

    public List<ProjectManage> getDeptIdPro() {

        return expenseMapper.getDeptIdPro();
    }

    public List<ProjectBean> getProId() {
        return expenseMapper.getProId();
    }

    public List<Map> getProjectDataByTime(String depno, String starttime, String endtime, String project_nunmber,int[] positionid) {

        if ("".equals(starttime)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            endtime = simpleDateFormat.format(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date); // 设置为当前时间
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1); // 设置为上一个月
            date = calendar.getTime();
            starttime = simpleDateFormat.format(date);
        }
        Map map = new HashMap();
        map.put("depno", depno);
        map.put("starttime", starttime);
        map.put("endtime", endtime);
        map.put("project_number", project_nunmber);
        if(positionid.length>0){
            map.put("positionid",positionid);
        }
        return expenseMapper.getProjectDataByTime(map);
    }
    public List<Map> getProjectDataByExpenseId(int[] expense_ids) {

        return expenseMapper.getProjectDataByExpenseId(expense_ids);
    }

    public Map<String, Object> getNewData(Expense expense, String endtime, String starttime) {


        Map map = new HashMap<String, Object>();
        Map map1 = new HashMap<String, Object>();
        Long depno = UserUtils.getCurrentHr().getDepartmentId();
        Long user_id = UserUtils.getCurrentHr().getId();

        map1.put("endtime", endtime);
        map1.put("starttime", starttime);
        map1.put("exp", expense);
        map1.put("depnum", depno);
        map1.put("user_id", user_id);
        List<Map<String, Object>> list = expenseMapper.getNewData(map1);

        map.put("newData", list);
        return map;
    }

    public Map<String, Object> getDetailTable(Expense expense, String starttime, String endtime,int[] positionid) {
        Map map = new HashMap<String, Object>();
        Map map1 = new HashMap();
        map1.put("endtime", endtime);
        map1.put("starttime", starttime);
        map1.put("exp", expense);
        if(positionid.length>0){
            map1.put("positionid",positionid);
        }
        List<Map<String, Object>> list = expenseMapper.getDetailTable(map1);
        map.put("detailData", list);
        return map;
    }

    public Map<String, Object> getDetailTableByExpenseIds(int[] expense_ids) {
        Map map = new HashMap<String, Object>();
        List<Map<String, Object>> list = expenseMapper.getDetailTableByExpenseIds(expense_ids);
        map.put("detailData", list);
        return map;
    }
    public Map<String, Object> getLoginUser() {
        Map map = new HashMap<String, Object>();
        User user = getCurrentHr();
        String userName = user.getName();
        int userId = expenseMapper.getUserId(userName);
        map.put("name", userName);
        map.put("id", userId);
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        list.add(map);
        Map map1 = new HashMap<String, Object>();
        map1.put("userName", list);
        return map1;
    }

    public List<Expense> getExpenseById(Expense expense) {

        return expenseMapper.getExpenseById(expense);
    }

    public List<Expense> getExUserName(Expense expense) {

        return expenseMapper.getExUserName(expense);
    }

    public List<Position> getPosition(){
        return expenseMapper.getPosition();
    }

    public List<Expense> getAppliId() {
        return expenseMapper.getAppliId();
    }

    public List<Expense> getAppliId1() {
        return expenseMapper.getAppliId1();
    }

    public List<Expense> getAduName() {

        return expenseMapper.getAduName();
    }

    public List<Expense> getExStu() {

        return expenseMapper.getExStu();
    }


    public List<Expense> getExType() {

        return expenseMapper.getExType();
    }

    public List<Expense> getUserData() {

        return expenseMapper.getUserData();
    }

    public int getPersonnalCount(Expense expense) {

        return expenseMapper.getPersonnalCount(expense);
    }

    public int updateExpenseFee1(Double eat_money, Expense expense) {

        return expenseMapper.updateExpenseFee1(eat_money, expense);
    }

    public int updateExpenseFee2(Double traffic_money, Expense expense) {

        return expenseMapper.updateExpenseFee2(traffic_money, expense);
    }

    public int updateExpenseFee3(Double extra_money, Expense expense) {

        return expenseMapper.updateExpenseFee3(extra_money, expense);
    }

    public int updateExpenseFee4(Double else_money, Expense expense) {

        return expenseMapper.updateExpenseFee4(else_money, expense);
    }

    public int addExpense(ExpensePo expensePo) {
        return expenseMapper.addExpense(expensePo);
    }

    public int addExpenseFee1(Double eat_money, int id) {

        return expenseMapper.addExpenseFee1(eat_money, id);
    }

    public int addExpenseFee2(Double traffic_money, int id) {

        return expenseMapper.addExpenseFee2(traffic_money, id);
    }

    public int addExpenseFee3(Double extra_money, int id) {

        return expenseMapper.addExpenseFee3(extra_money, id);
    }

    public int addExpenseFee4(Double else_money, int id) {

        return expenseMapper.addExpenseFee4(else_money, id);
    }

    public int deleteExpenseFee1(Expense expense) {

        return expenseMapper.deleteExpenseFee1(expense);
    }

    public int deleteExpenseFee2(Expense expense) {

        return expenseMapper.deleteExpenseFee2(expense);
    }

    public int deleteExpenseFee3(Expense expense) {

        return expenseMapper.deleteExpenseFee3(expense);
    }

    public int deleteExpenseFee4(Expense expense) {

        return expenseMapper.deleteExpenseFee4(expense);
    }

    public int selectBeforeUpdate1(Expense expense) {
        int total1 = expenseMapper.selectBeforeUpdate1(expense);
        return total1;
    }

    public int selectBeforeUpdate2(Expense expense) {
        int total2 = expenseMapper.selectBeforeUpdate2(expense);
        return total2;
    }

    public int selectBeforeUpdate3(Expense expense) {
        int total3 = expenseMapper.selectBeforeUpdate3(expense);
        return total3;
    }

    public int selectBeforeUpdate4(Expense expense) {
        int total4 = expenseMapper.selectBeforeUpdate4(expense);
        return total4;
    }

    public int updateExById(Expense expense) {

        return expenseMapper.updateExById(expense);
    }

    public int auditById(Expense expense, String username) {
        return expenseMapper.auditById(expense, username);
    }

    public boolean deleteExById(String pids) {
        String[] split = pids.split(",");
        return expenseMapper.deleteExById(split) == split.length;
    }

    public boolean deleteExFeeById(String pids) {
        String[] split = pids.split(",");
        return expenseMapper.deleteExFeeById(split) == split.length;
    }

    public RespBean audit(int[] ids, String expCom) {
        RespBean respBean = new RespBean();
        if ("".equals(expCom)) {
            expCom = "通过";
        }
        try{
            for(int i = 0;i<ids.length;i++){
                Map map = new HashMap();
                map.put("expense_id", ids[i]);
                map.put("expCom", expCom);
                map.put("name", UserUtils.getCurrentHr().getName());
                expenseMapper.auditApplication(map);
                expenseMapper.audit(map);
            }
            respBean.setStatus("success");
            respBean.setMsg("成功");
        }catch (Exception e){
            respBean.setMsg("出错,请联系管理员");
            respBean.setStatus("error");
            e.printStackTrace();
        }

        return respBean;
    }

    public RespBean callback(int[] ids, String expCom) {
        RespBean respBean = new RespBean();
        try{
            for(int i =0;i<ids.length;i++){
                Map map = new HashMap();
                map.put("expense_id", ids[i]);
                map.put("expCom", expCom);
                map.put("name", UserUtils.getCurrentHr().getName());
                expenseMapper.callback(map);
            }
            respBean.setStatus("success");
            respBean.setMsg("打回成功");
        }catch (Exception e){
            respBean.setMsg("打回出错,请联系管理员");
            respBean.setStatus("error");
            e.printStackTrace();
        }
        return respBean;
    }

    public boolean isAuditor() {
        if (expenseMapper.isAuditor(UserUtils.getCurrentHr().getId()).size() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
