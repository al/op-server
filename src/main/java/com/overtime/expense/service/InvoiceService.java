package com.overtime.expense.service;

import com.exception.AppException;
import com.overtime.expense.bean.Invoice;
import com.overtime.expense.mapper.InvoiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhong on 2018/8/20.
 */
@Service
@Transactional
public class InvoiceService {
    @Autowired
    InvoiceMapper invoiceMapper;
    public List<Invoice> getAllInvoice(long userid){
        return invoiceMapper.getAllInvoice(userid);
    }
    public List<Invoice> getInvoice(long userid,Integer start,Integer pageSize){
        Map map = new HashMap();
        map.put("start",start);
        map.put("pageSize",pageSize);
        map.put("userid",userid);
        return invoiceMapper.getInvoice(map);
    }
    public int deleteInvoice(String invoice_id){
        return invoiceMapper.deleteInvoice(invoice_id);
    }

    public int updateInvoice(Invoice invoice){
        int i = invoiceMapper.updateInvoice(invoice);
        try{
            if(i==1){
                return i;
            }else{
                throw new AppException("修改失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public int insertInvoice(Invoice invoice){
        return invoiceMapper.insertInvoice(invoice);
    }

    public int insertPicture(Map map){
        return invoiceMapper.insertPicture(map);
    }

    public List<Invoice> getInvoiceByName(Map map){
        return invoiceMapper.getInvoiceByName(map);
    }
}
