package com.base.bean;

public class AA10 {
    private String aaa100;
    private String aaa101;
    private String aaa102;
    private String aaa103;
    private String yab003;

    public String getAaa100() {
        return aaa100;
    }
    public void setAaa100(String aaa100) {
        this.aaa100 = aaa100;
    }

    public String getAaa101() { return aaa101; }
    public void setAaa101(String aaa101) {
        this.aaa101 = aaa101;
    }

    public String getAaa102() {
        return aaa102;
    }
    public void setAaa102(String aaa102) {
        this.aaa102 = aaa102;
    }

    public String getAaa103() {
        return aaa103;
    }
    public void setAaa103(String aaa103) {
        this.aaa103 = aaa103;
    }

    public String getYab003() {
        return yab003;
    }
    public void setYab003(String yab003) {
        this.yab003 = yab003;
    }

}
