package com.base.service;

import com.base.bean.Menu;
import com.base.bean.RespBean;
import com.base.mapper.MenuRoleMapper;
import com.common.OPConst;
import com.common.UserUtils;
import com.base.mapper.MenuMapper;
import com.common.tree.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.management.openmbean.OpenDataException;
import java.util.List;

@CacheConfig(cacheNames = "menu")
@Service
@Transactional
public class MenuService {
    @Autowired
    MenuMapper menuMapper;
    @Autowired
    MenuRoleMapper menuRoleMapper;

    @Cacheable(key = "'getAllMenu'")
    public List<Menu> getAllMenu() {
        return menuMapper.getAllMenu();
    }

    public List<Menu> getMenusByHrId() {
        return menuMapper.getMenusByHrId(UserUtils.getCurrentHr().getId());
    }

    public List<Menu> menuTree() {
        return menuMapper.menuTree();
    }

    public List<Long> getMenusByRid(Long rid) {
        return menuMapper.getMenusByRid(rid);
    }

    public List<TreeNode> getTreeNode() {
        return menuMapper.getTreeNode();
    }

    @CacheEvict(key = "'getAllMenu'")
    public int deleteMenus(long menuId) {
        Menu menu = menuMapper.queryMenuById(menuId);
        String code = menu.getCode();
        List<Menu> list = menuMapper.queryMenuByCode(code);
        menuRoleMapper.deleteByMenus(list);
        int num = menuMapper.deleteMenuByCode(code);
        return num;
    }

    public Menu queryMenuById(long id) {
        return menuMapper.queryMenuById(id);
    }

    @CacheEvict(key = "'getAllMenu'")
    public RespBean addMenu(Menu menu) {
        RespBean respBean = new RespBean();
        if (menu == null) {
            respBean.setMsg("参数为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        if (StringUtils.isEmpty(menu.getUrl())) {
            respBean.setMsg("菜单url为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        if (StringUtils.isEmpty(menu.getComponent())) {
            respBean.setMsg("菜单组件为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        if (StringUtils.isEmpty(menu.getName())) {
            respBean.setMsg("菜单名称为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        if (StringUtils.isEmpty(menu.getParentId())) {
            respBean.setMsg("菜单父级ID为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        Menu pMenu = menuMapper.queryMenuById(menu.getParentId());
        menuMapper.addMenu(menu);
        menu.setId(menu.getId());
        menu.setCode(pMenu.getCode() + "_" + menu.getId());
        menuMapper.updateMenu(menu);
        respBean.setStatus(OPConst.RESP_TYPE_SUCCESS.getValue());
        respBean.setMsg("新增菜单成功");
        respBean.setResult(menu.getId() + "");
        return respBean;
    }

    @CacheEvict(key = "'getAllMenu'")
    public RespBean updateMenu(Menu menu) {
        RespBean respBean = new RespBean();
        if (menu == null) {
            respBean.setMsg("参数为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        if (StringUtils.isEmpty(menu.getId())) {
            respBean.setMsg("菜单ID为空");
            respBean.setStatus(OPConst.RESP_TYPE_ERROR.getValue());
            return respBean;
        }
        menuMapper.updateMenu(menu);
        respBean.setMsg("更新菜单成功");
        return respBean;
    }
}
