package com.base.service;

import com.base.bean.AA10;
import com.base.mapper.AA10Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AA10Service {
    @Autowired
    AA10Mapper aa10Mapper;

    public int addCode(Map map){
        return aa10Mapper.addCode(map);
    }
    public int editCode(Map map){
        return aa10Mapper.editCode(map);
    }
    public List getCode(){
        return aa10Mapper.getCode();
    }

    public int deleteCode(Map map) {
        return aa10Mapper.deleteCode(map);
    }
}
