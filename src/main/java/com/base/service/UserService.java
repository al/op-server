package com.base.service;

import com.base.bean.User;
import com.base.mapper.UserMapper;
import com.common.OPConst;
import com.common.UserUtils;
import com.exception.AppException;
import com.exception.ExceptionConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService implements UserDetailsService {

    @Autowired
    UserMapper hrMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User hr = hrMapper.loadUserByUsername(s);
        if (hr == null) {
            throw new UsernameNotFoundException("用户名不对");
        }
        return hr;
    }

    public int hrReg(String username, String password) {
        //如果用户名存在，返回错误
        if (hrMapper.loadUserByUsername(username) != null) {
            return -1;
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(password);
        return hrMapper.hrReg(username, encode);
    }

    public List<User> getHrsByKeywords(String keywords, Integer start,  Integer pageSize) {
        return hrMapper.getHrsByKeywords(keywords, start, pageSize);
    }

    public Integer getHrsByKeywordsCount(String keywords) {
        return hrMapper.getHrsByKeywordsCount(keywords);
    }

    public int updateHr(User hr) {
        int n = -1;
        try {
            n = hrMapper.updateHr(hr);
            return n;
        } catch (Exception e) {
            e.printStackTrace();
            if(e instanceof DuplicateKeyException) {
                throw new AppException(ExceptionConst.EXCEPTION_CODE_0001.getCode(), ExceptionConst.EXCEPTION_CODE_0001.getMessage());
            } else {
                throw new AppException(ExceptionConst.EXCEPTION_CODE_0000.getCode(), ExceptionConst.EXCEPTION_CODE_0000.getMessage());
            }

        }
    }

    public int addUser(User user) {
        user.setUserface(OPConst.DEFAULT_HEAD.getValue());
        String password = user.getPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(password);
        user.setPassword(encode);
        int n = -1;
        try {
            n = hrMapper.addUser(user);
            return n;
        } catch (Exception e) {
            e.printStackTrace();
            if(e instanceof DuplicateKeyException) {
                throw new AppException(ExceptionConst.EXCEPTION_CODE_0001.getCode(), ExceptionConst.EXCEPTION_CODE_0001.getMessage());
            } else {
                throw new AppException(ExceptionConst.EXCEPTION_CODE_0000.getCode(), ExceptionConst.EXCEPTION_CODE_0000.getMessage());
            }

        }
    }

    public int resetPassword(Long userId) {
        User user = new User();
        user.setId(userId);
        String password = OPConst.DEFAULT_PASSWORD.getValue();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(password);
        user.setPassword(encode);
        return hrMapper.updateHr(user);
    }

    public int updateHrRoles(Long hrId, Long[] rids) {
        int i = hrMapper.deleteRoleByHrId(hrId);
        return hrMapper.addRolesForHr(hrId, rids);
    }

    public User getHrById(Long hrId) {
        return hrMapper.getHrById(hrId);
    }

    public User getByUserId(Long userId) {
        return hrMapper.getByUserId(userId);
    }

    public int deleteHr(Long hrId) {
        hrMapper.deleteRoleByHrId(hrId);
        return hrMapper.deleteHr(hrId);
    }

    public List<User> getAllHrExceptAdmin() {
        return hrMapper.getAllHr(UserUtils.getCurrentHr().getId());
    }
    public List<User> getAllHr() {
        return hrMapper.getAllHr(null);
    }

    public int changePwd(String newpass, long id) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = encoder.encode(newpass);
        return hrMapper.changePwd(password,id);
    }
}
