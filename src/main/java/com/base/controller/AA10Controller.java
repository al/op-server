package com.base.controller;

import com.base.bean.AA10;
import com.base.bean.RespBean;
import com.base.service.AA10Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/aa10")
public class AA10Controller {

    @Autowired
    AA10Service aa10Service;

    @RequestMapping("/addCode")
    public RespBean addCode(String aaa100,String aaa101,String aaa102,String aaa103){
        Map map = new HashMap();
        map.put("aaa100",aaa100);
        map.put("aaa101",aaa101);
        map.put("aaa102",aaa102);
        map.put("aaa103",aaa103);
        if(aa10Service.addCode(map)==1){
            return new RespBean("success","新增成功");
        }else{
            return new RespBean("error","新增失败");
        }
    }

    @RequestMapping("/editCode")
    public RespBean editCode(String aaa100,String aaa101,String aaa102,String aaa103){
        Map map = new HashMap();
        map.put("aaa100",aaa100);
        map.put("aaa101",aaa101);
        map.put("aaa102",aaa102);
        map.put("aaa103",aaa103);
        if(aa10Service.editCode(map)==1){
            return new RespBean("success","修改成功");
        }else{
            return new RespBean("error","修改失败");
        }
    }
    @RequestMapping("/getCode")
    public List getCode(){
        return aa10Service.getCode();
    }

    @RequestMapping("/deleteCode")
    public RespBean deleteAL(String aaa100,String aaa102){
        Map map = new HashMap();
        map.put("aaa100",aaa100);
        map.put("aaa102",aaa102);
        if(aa10Service.deleteCode(map)==1){
            return new RespBean("success","删除成功");
        }else{
            return new RespBean("error","删除失败");
        }
    }

}
