package com.base.controller;

import com.base.bean.Menu;
import com.base.bean.RespBean;
import com.base.service.MenuService;
import com.common.OPConst;
import com.common.tree.TreeBuilder;
import com.common.tree.TreeNode;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/system/menu")
public class MenuController {
    @Autowired
    MenuService menuService;

    @RequestMapping(value = "/menuTree", method = RequestMethod.GET)
    public List<TreeNode> menuTree() {
        return TreeBuilder.build(menuService.getTreeNode());
    }

    @RequestMapping(value = "/menuDelete", method = RequestMethod.DELETE)
    public RespBean menuDelete(@Param("menuId") Integer menuId) {
        RespBean respBean = new RespBean();
        int n = menuService.deleteMenus(menuId);
        if(n >= 1) {
            respBean.setMsg("删除成功！");
            respBean.setStatus(OPConst.STATUS_1.getValue());
        } else {
            respBean.setMsg("删除失败！");
            respBean.setStatus(OPConst.STATUS_0.getValue());
        }
        return respBean;
    }

    @RequestMapping(value = "/queryMenuById", method = RequestMethod.GET)
    public Menu queryMenuById(@Param("id") long id) {
        return menuService.queryMenuById(id);
    }

    @RequestMapping(value = "/menuAdd", method = RequestMethod.POST)
    public RespBean menuAdd(@Param("menu") Menu menu) {
        return menuService.addMenu(menu);
    }

    @RequestMapping(value = "/menuUpdate", method = RequestMethod.POST)
    public RespBean menuUpdate(@Param("menu") Menu menu) {
        return menuService.updateMenu(menu);
    }


}
