package com.base.mapper;

import com.base.bean.Menu;
import com.common.tree.TreeNode;

import java.util.List;


public interface MenuMapper {
    List<Menu> getAllMenu();

    List<Menu> getMenusByHrId(Long hrId);

    List<Menu> menuTree();

    List<Long> getMenusByRid(Long rid);

    Menu queryMenuById(long id);

    Menu queryMenuByParentId(long id);

    List<TreeNode> getTreeNode();

    List<Menu> queryMenuByCode(String code);

    int deleteMenuByCode(String code);

    int addMenu(Menu menu);

    int updateMenu(Menu menu);
}
