package com.base.mapper;

import org.apache.ibatis.annotations.Param;
import com.base.bean.User;
import com.base.bean.Role;

import java.util.List;

public interface UserMapper {
    User loadUserByUsername(String username);

    List<Role> getRolesByHrId(Long id);

    int hrReg(@Param("username") String username, @Param("password") String password);

    List<User> getHrsByKeywords(@Param("keywords") String keywords, @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    public Integer getHrsByKeywordsCount(@Param("keywords") String keywords);

    int updateHr(User hr);

    int addUser(User user);

    int deleteRoleByHrId(Long hrId);

    int addRolesForHr(@Param("hrId") Long hrId, @Param("rids") Long[] rids);

    User getHrById(Long hrId);

    User getByUserId(Long userId);

    int deleteHr(Long hrId);

    List<User> getAllHr(@Param("currentId") Long currentId);

    int  changePwd (@Param("password") String password,@Param("id") long id);

}
