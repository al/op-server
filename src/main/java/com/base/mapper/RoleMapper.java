package com.base.mapper;

import org.apache.ibatis.annotations.Param;
import com.base.bean.Role;

import java.util.List;
import java.util.Map;


public interface RoleMapper {
    List<Role> roles();

    int addNewRole(@Param("role") String role, @Param("roleZh") String roleZh);

    int deleteRoleById(Long rid);

    int editRole(Map map);
}
