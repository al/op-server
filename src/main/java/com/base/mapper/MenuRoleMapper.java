package com.base.mapper;

import com.base.bean.Menu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface MenuRoleMapper {

    int deleteMenuByRid(@Param("rid") Long rid);

    int addMenu(@Param("rid") Long rid, @Param("mids") Long[] mids);

    int deleteByMenus(@Param("menus") List<Menu> menus);
}
