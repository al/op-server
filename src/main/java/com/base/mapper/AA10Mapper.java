package com.base.mapper;

import com.base.bean.AA10;

import java.util.List;
import java.util.Map;

public interface AA10Mapper {
    int addCode(Map map);
    int editCode(Map map);
    List getCode();
    int deleteCode(Map map);
}
