package com.base.mapper;

import org.apache.ibatis.annotations.Param;
import com.base.bean.User;
import com.base.bean.MsgContent;
import com.base.bean.SysMsg;

import java.util.List;

public interface SysMsgMapper {

    int sendMsg(MsgContent msg);

    int addMsg2AllHr(@Param("hrs") List<User> hrs, @Param("mid") Long mid);

    List<SysMsg> getSysMsg(@Param("start") int start, @Param("size") Integer size,@Param("hrid") Long hrid);

    int markRead(@Param("flag") Long flag, @Param("hrid") Long hrid);
}
