package com.common.tree;

import java.util.List;

/**
 * @author yt
 * @createtime 2018-08-09 08:41:42
 * @description
 */
public class TreeNode {

    private Integer id;

    private Integer parentId;

    private String label;

    private List<TreeNode> children;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "id='" + id + '\'' +
                ", parentId='" + parentId + '\'' +
                ", label='" + label + '\'' +
                ", children=" + children +
                '}';
    }
}

