package com.common;

/**
 * @author yt
 * @createtime 2018-08-10 11:11:00
 * @description
 */
public enum OPConst {

    STATUS_0("0", "失败"),
    STATUS_1("1", "成功"),
    RESP_TYPE_INFO("info", "info"),
    RESP_TYPE_SUCCESS("success", "success"),
    RESP_TYPE_WARNING("warning", "warning"),
    RESP_TYPE_ERROR("error", "error"),
    DEFAULT_HEAD("http://192.168.17.212:8081/ftpfile/default_head.jpg", "默认头像"),
    HEAD_DOMAIN("http://192.168.17.212:8081/", "头像路径"),
    INVOICE_PATH("http://192.168.17.212:8081/","发票地址"),
    DEFAULT_INVOICE("http://192.168.17.212:8081/default_invoice.jpg","默认发票图片"),
    DEFAULT_PASSWORD("123456", "默认密码")

    ;

    private String value;
    private String name;

    OPConst(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
